#include "pch.h"
#include "ScreenTransition.h"
using namespace DirectX;

ScreenTransition::ScreenTransition()
{
	active = false;
	fadeIn = false;
	frameCountBlack = 0;
	alphaValue = 0.f;
	alphaDelta = .02f;
	m_states = nullptr;
	m_effect = nullptr;
	m_inputLayout = nullptr;
}


ScreenTransition::~ScreenTransition()
{
}

void ScreenTransition::Init(ID3D11Device1* dev)
{
	m_states = std::make_unique<CommonStates>(dev);

	m_effect = std::make_unique<BasicEffect>(dev);
	m_effect->SetVertexColorEnabled(true);

	void const* shaderByteCode;
	size_t byteCodeLength;

	m_effect->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

	DX::ThrowIfFailed(
		dev->CreateInputLayout(VertexPositionColor::InputElements,
			VertexPositionColor::InputElementCount,
			shaderByteCode, byteCodeLength,
			m_inputLayout.ReleaseAndGetAddressOf()));
}

void ScreenTransition::Render(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* pb, ID3D11DeviceContext1* context)
{
	context->OMSetBlendState(m_states->AlphaBlend(), Colors::Black, 0xffffffff);
	context->OMSetDepthStencilState(m_states->DepthNone(), 0);
	context->RSSetState(m_states->CullNone());

	m_effect->Apply(context);

	context->IASetInputLayout(m_inputLayout.Get());
	pb->Begin();
	VertexPositionColor v1(SimpleMath::Vector3(-1.f, -1.f, 0.f), SimpleMath::Vector4(0.0f, 0.0f, 0.0f, alphaValue)); // top left
	VertexPositionColor v2(SimpleMath::Vector3(1.f, -1.f, 0.f), SimpleMath::Vector4(0.0f, 0.0f, 0.0f, alphaValue));// top right
	VertexPositionColor v3(SimpleMath::Vector3(1.f, 1.f, 0.f), SimpleMath::Vector4(0.0f, 0.0f, 0.0f, alphaValue)); // bottom right
	VertexPositionColor v4(SimpleMath::Vector3(-1.f, 1.f, 0.f), SimpleMath::Vector4(0.0f, 0.0f, 0.0f, alphaValue)); // bottom left
		
	pb->DrawQuad(v1, v2, v3, v4);
	pb->End();
}
