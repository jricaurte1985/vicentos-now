#include "pch.h"
#include "Camera.h"

Camera::Camera() {};

Camera::Camera(float w, float h, float mapW, float mapH, RECT mar, DirectX::SimpleMath::Vector3 p, DirectX::SimpleMath::Vector3 foc,
				float sp, bool t, bool b, bool l , bool r) :
	camWidth(w), camHeight(h), mapWidth(mapW), mapHeight(mapH), margins(mar), pos(p), focus(foc), speed(sp), 
	topLocked(t), bottomLocked(b), leftLocked(l), rightLocked(r)
{
	world = DirectX::SimpleMath::Matrix::Identity;
	margins.top = (camHeight/ 2.f) + speed;
	margins.bottom = (mapHeight - margins.top) - speed;
	margins.left = (camWidth / 2.f) + speed;
	margins.right = (camWidth - margins.left) - speed;
}

void Camera::SetLocks()
{
	if (pos.y <= margins.top)
		topLocked = true;
	else
		topLocked = false;

	if (pos.y >= margins.bottom)
		bottomLocked = true;
	else
		bottomLocked = false;
	
	if (pos.x <= margins.left)
		leftLocked = true;
	else
		leftLocked = false;

	if (pos.x >= margins.right)
		rightLocked = true;
	else
		rightLocked = false;
}

void Camera::UpdateMargins(float w, float h, float mapW, float mapH)
{
	margins.top = (h / 2.f) + speed;
	margins.bottom = (mapH - margins.top) - speed;
	margins.left = (w / 2.f) + speed;
	margins.right = (mapW - margins.left) - speed;
}

void Camera::OnWindowSizeChange(float w, float h, float mapW, float mapH)
{
	SetLocks();
	float diffx, diffy;
	diffx = (w - camWidth)/2.f;
	diffy = (h - camHeight)/2.f;

	if (topLocked && h > camHeight)
	{
		margins.top = (h / 2.f);
		pos.y += diffy;
		focus.y += diffy;
	}
	else if (topLocked && h < camHeight)
	{
		margins.top = (h / 2.f);
		pos.y = margins.top;
		focus.y = margins.top;
	}
	else	
		margins.top = (h / 2.f);

	if (bottomLocked && h > camHeight)
	{
		margins.bottom = (mapH - (h / 2.f));
		pos.y -= diffy;
		focus.y -= diffy;
	}		
	else if (bottomLocked && h < camHeight)
	{
		margins.bottom = (mapH - (h / 2.f));
		pos.y = margins.bottom;
		focus.y = margins.bottom;
	}
	else
		margins.bottom = (mapH - (h / 2.f));

	if (leftLocked && w > camWidth)
	{
		margins.left = (w / 2.f);
		pos.x += diffx;
		focus.x += diffx;
	}
	else if (leftLocked && w < camWidth)
	{
		margins.left = (w / 2.f);
		pos.x = margins.left;
		focus.x = margins.left;
	}
	else
		margins.left = (w / 2.f);

	if (rightLocked && w > camWidth)
	{
		margins.right = ((mapW - (w / 2.f)));
		pos.x -= diffx;
		focus.x -= diffx;
	}
	else if (rightLocked && w < camWidth)
	{
		margins.right = ((mapW - (w / 2.f)));
		pos.x -= margins.right;
		focus.x -= margins.right;
	}
	else
		margins.right = ((mapW - (w / 2.f)));


	camWidth = w;
	camHeight = h;
	UpdateCamera();
	
}

void Camera::OnWindowSizeChange(float cW, float cH, float offset)
{
		camWidth = cW;
		camHeight = cH;
		pos.x = (cW / 2.f) + offset;
		focus.x = (cW / 2.f) + offset;
		pos.y = (cH / 2.f) + offset;
		focus.y = (cH / 2.f) + offset;
		UpdateCamera();
}

void Camera::InitializeCamera(float w, float h, float mapW, float mapH, DirectX::SimpleMath::Vector3 position, DirectX::SimpleMath::Vector3 focusPoint, float sp)
{
	camWidth = w;
	camHeight = h;
	mapWidth = mapW;
	mapHeight = mapH;
	pos = position;
	focus = focusPoint;
	speed = sp;
	topLocked = false;
	bottomLocked = false;
	leftLocked = false;
	rightLocked = false;
	world = DirectX::XMMatrixIdentity();

	//+- speed or else extra pixels show
	margins.top = (h / 2.f);
	margins.bottom = (mapH - (h / 2.f));
	margins.left = (w / 2.f);
	margins.right = (mapW - (w / 2.f));

	UpdateCamera();
}

void Camera::UpdateCamera()
{
	proj = DirectX::XMMatrixOrthographicLH(camWidth, camHeight, 1.f, 1000.f);
	view = DirectX::XMMatrixLookAtLH(pos, focus, DirectX::SimpleMath::Vector3(0.f, -1.f, 0.f));
	wvp = world * view * proj;
}