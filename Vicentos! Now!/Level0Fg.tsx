<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Foreground" tilewidth="1500" tileheight="1409" tilecount="20" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="400" height="562" source="../../../../Desktop/temp/ForeGround/bushyTreeFinal.png"/>
  <objectgroup draworder="index">
   <object id="1" x="162" y="302" width="68" height="72"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <image width="700" height="700" source="../../../../Desktop/temp/ForeGround/desertTent.png"/>
  <objectgroup draworder="index">
   <object id="1" x="12" y="216" width="278" height="448"/>
   <object id="3" x="405" y="216" width="278" height="448"/>
   <object id="4" x="288" y="214" width="124" height="284"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="1500" height="850" source="../../../../Desktop/temp/ForeGround/fightersInn.png"/>
  <objectgroup draworder="index">
   <object id="1" x="60" y="332" width="1412" height="288"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="350" height="525" source="../../../../Desktop/temp/ForeGround/forestTreeFinal.png"/>
  <objectgroup draworder="index">
   <object id="1" x="148" y="332" width="40" height="88"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <image width="74" height="60" source="../../../../Desktop/temp/ForeGround/greenPlant1.png"/>
 </tile>
 <tile id="5">
  <image width="70" height="68" source="../../../../Desktop/temp/ForeGround/greenPlant2.png"/>
 </tile>
 <tile id="6">
  <image width="369" height="483" source="../../../../Desktop/temp/ForeGround/miniTent.png"/>
 </tile>
 <tile id="7">
  <image width="46" height="67" source="../../../../Desktop/temp/ForeGround/orangePlant1.png"/>
 </tile>
 <tile id="8">
  <image width="45" height="49" source="../../../../Desktop/temp/ForeGround/orangePlant2.png"/>
 </tile>
 <tile id="9">
  <image width="500" height="536" source="../../../../Desktop/temp/ForeGround/prettyTreeFinal.png"/>
  <objectgroup draworder="index">
   <object id="1" x="212" y="292" width="76" height="100"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <image width="87" height="93" source="../../../../Desktop/temp/ForeGround/redPlant.png"/>
 </tile>
 <tile id="11">
  <image width="79" height="86" source="../../../../Desktop/temp/ForeGround/redPlant2.png"/>
 </tile>
 <tile id="12">
  <image width="75" height="80" source="../../../../Desktop/temp/ForeGround/redPlant3.png"/>
 </tile>
 <tile id="13">
  <image width="80" height="81" source="../../../../Desktop/temp/ForeGround/redPlant4.png"/>
 </tile>
 <tile id="14">
  <image width="870" height="797" source="../../../../Desktop/temp/ForeGround/smallBlueHouse.png"/>
  <objectgroup draworder="index">
   <object id="1" x="108" y="232" width="684" height="168"/>
   <object id="2" x="80" y="696" width="204" height="28"/>
   <object id="3" x="410" y="682" width="396" height="28"/>
   <object id="4" x="84" y="484" width="24" height="232"/>
   <object id="5" x="788" y="412" width="24" height="260"/>
  </objectgroup>
 </tile>
 <tile id="15">
  <image width="320" height="500" source="../../../../Desktop/temp/ForeGround/smallerTree.png"/>
  <objectgroup draworder="index">
   <object id="1" x="140" y="364" width="44" height="72"/>
  </objectgroup>
 </tile>
 <tile id="16">
  <image width="450" height="630" source="../../../../Desktop/temp/ForeGround/smallStoneHouse.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="320" width="448" height="204"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <image width="1024" height="1409" source="../../../../Desktop/temp/ForeGround/spookyTree.png"/>
  <objectgroup draworder="index">
   <object id="1" x="320" y="556" width="220" height="780"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <image width="400" height="600" source="../../../../Desktop/temp/ForeGround/tallTree.png"/>
  <objectgroup draworder="index">
   <object id="1" x="180" y="376" width="60" height="108"/>
  </objectgroup>
 </tile>
 <tile id="19">
  <image width="750" height="968" source="../../../../Desktop/temp/ForeGround/twoStoryBlueHouse.png"/>
  <objectgroup draworder="index">
   <object id="1" x="8" y="308" width="732" height="432"/>
  </objectgroup>
 </tile>
</tileset>
