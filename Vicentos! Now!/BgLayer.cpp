#include"pch.h"
#include "BgLayer.h"

using namespace DirectX;

void BgLayer::Load(int idd, DataResource& dr, ID3D11Device1* dev)
{
	tiles.clear();
	bb.clear();
	drawOrder.clear();
	ss.ssType = 2;
	mapSize.x = 0.f;
	mapSize.y = 0.f;
	tileSize.x = 0.f;
	tileSize.y = 0.f;

	id = idd;

	//load spriteSheet txt file
	ss.Load(id, dev, dr);

	//load Tiled map file, populate tiles vector
	pugi::xml_document tmx;

	if (tmx.load_file(dr.mapTMXFiles[id]))
	{
		int width = tmx.child("map").attribute("width").as_int();
		int height = tmx.child("map").attribute("height").as_int();
		tileSize.x = tmx.child("map").attribute("tilewidth").as_float();
		tileSize.y = tmx.child("map").attribute("tileheight").as_float();
		mapSize.x = static_cast<float>(width) * tileSize.x;
		mapSize.y = static_cast<float>(height) * tileSize.y;
		
		
		std::string line = tmx.child("map").child("layer").child("data").child_value();
		std::istringstream sstream(line);
		std::string token;
		while (std::getline(sstream, token, ',')) // Read each word seperated by spaces
		{
			int ssIndex = std::stoi(token) - 1;
			drawOrder.emplace_back(ssIndex);
		}
		
		float tempX = 0.f, tempY = 0.f;
		int idCount = 0;
		for (unsigned int y = 0; y < height; y++)
		{
			for (unsigned int x = 0; x < width; x++)
			{
				Tile tile;
				tile.pos.x = tempX;
				tile.origin.x = tempX + (tileSize.x / 2.f);
								
				tile.pos.y = tempY;
				tile.origin.y = tempY + (tileSize.y / 2.f);
				tile.id = idCount;
				tile.ssIndex = drawOrder[idCount];

				tiles.emplace_back(tile);
				++idCount;
				tempX += tileSize.x;
			}
			tempY += tileSize.y;
			tempX = 0.f;
		}
	}

	//load tsx file, populate bounding boxes
	pugi::xml_document tsx;
	if (tsx.load_file(dr.mapBgTSXFiles[id]))
	{
		pugi::xml_node bbTiles = tsx.child("tileset");
		int bbID = 0;
		int numTiles = tiles.size();

		DirectX::XMFLOAT2 bbSize(0.f, 0.f), bbOrigin(0.f, 0.f);
		for (pugi::xml_node tile : bbTiles.children("tile"))
		{
			int ssIdx = tile.attribute("id").as_int();
			pugi::xml_node objects = tile.child("objectgroup");
			for (pugi::xml_node obj : objects.children("object"))
			{
				bbSize.x = obj.attribute("width").as_float();
				bbSize.y = obj.attribute("height").as_float();
				float xPos = obj.attribute("x").as_float();
				float yPos = obj.attribute("y").as_float();
				float halfWidth = bbSize.x / 2.f;
				float halfHeight = bbSize.y / 2.f;
				for (int i = 0; i < numTiles; i++)
				{
					if (tiles[i].ssIndex == ssIdx)
					{
						int tileId = BgLayer::tiles[i].id;
						bbOrigin.x = BgLayer::tiles[i].pos.x + xPos + halfWidth;
						bbOrigin.y = BgLayer::tiles[i].pos.y + yPos + halfHeight;
						bb.emplace_back(bbOrigin, bbSize, bbID, tileId, ssIdx, true);
						++bbID;
					}
				}				
				
			}
		}
	}
}

void BgLayer::Render(DirectX::SpriteBatch* batch)
{
	unsigned int size = tiles.size();
	for (unsigned int i = 0; i < size; i++)
	{
		ss.Draw(batch, ss.ssFrames[drawOrder[i]], tiles[i].origin, DirectX::Colors::White, 0.f, 1.f);
	}
}


