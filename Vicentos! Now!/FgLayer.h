#pragma once
#include "FgObject.h"

class FgLayer
{
public:
	SpriteSheet ss;
	//background layer, foreground layer, object layer
	std::vector<FgObject> fgObjects;
	std::vector<int> drawOrder;
	//Bounding Boxes
	std::vector<BB> bb;
	int id;

	FgLayer();
	~FgLayer();
	void Render(DirectX::SpriteBatch* batch);
	void Load(int idd, DataResource& dr, ID3D11Device1* dev);
};

