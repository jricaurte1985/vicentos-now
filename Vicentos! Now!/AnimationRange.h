#pragma once
#include <string>
struct AnimationRange
{
	std::vector<DirectX::XMFLOAT2> posOffsets;
	std::vector<char> hitFrames, soundIndices;
	DirectX::XMFLOAT2 cycleOffset;
	std::string name;
	int min, max;
	float animRefresh;

	AnimationRange();
	AnimationRange(std::vector<DirectX::XMFLOAT2> offsets, std::vector<char> hFrames, std::vector<char> sounds, DirectX::XMFLOAT2 cycOff, std::string n, int mi , int ma, float ref);
};