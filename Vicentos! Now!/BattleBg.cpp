#include "pch.h"
#include "BattleBg.h"


BattleBg::BattleBg()
{
	id = -1;
	size.x = 1928.f;
	size.y = 1088.f;
	origin.x = 964.f;
	origin.y = 544.f;

	LeftPos1.x = 530.f;
	LeftPos1.y = 750.f;
	LeftPos2.x = 336.f;
	LeftPos2.y = 914.f;
	LeftPos3.x = 216.f;
	LeftPos3.y = 636.f;

	RightPos1.x = 1390.f;
	RightPos1.y = 750.f;
	RightPos2.x = 1584.f;
	RightPos2.y = 914.f;
	RightPos3.x = 1704.f;
	RightPos3.y = 636.f;
}


BattleBg::~BattleBg()
{
	mTexture.Reset();
}

void BattleBg::Load(int i, DataResource dr, ID3D11Device1* dev)
{
	id = i;
	DX::ThrowIfFailed(DirectX::CreateDDSTextureFromFile
								(
									dev, dr.battleBgDDSFiles[id], 
									nullptr, 
									mTexture.ReleaseAndGetAddressOf()
								));
}
