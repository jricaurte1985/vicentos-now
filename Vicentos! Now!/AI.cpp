#include "pch.h"
#include "AI.h"

AI::AI()
{
	id = -1;
}


AI::~AI()
{
}

void AI::Load(int i, DataResource& dr)
{
	id = i;
	std::ifstream fs(dr.spriteAnimFiles[id]);
	if (fs)
	{

		while (!fs.eof()) // Read each word seperated by spaces
		{
			std::string name;
			std::string x;
			std::string y;
			std::string hitFrames;
			std::string sounds;
			float animRefresh = 0.f;
			int min = 0, max = 0;

			fs >> name >> min >> max >> animRefresh;
			if (name == "*")
				break;
			std::getline(fs, x);
			std::getline(fs, x);
			std::getline(fs, y);

			std::istringstream sstr(x);
			std::string token;
			DirectX::XMFLOAT2 delta;
			std::vector<DirectX::XMFLOAT2> offsets;
			int count = 0;
			while (std::getline(sstr, token, ' ')) // Read each word seperated by spaces
			{
				delta.x = std::stof(token);
				offsets.emplace_back(delta);				
			}
			sstr.clear();
			sstr.str(y);
			while (std::getline(sstr, token, ' ')) // Read each word seperated by spaces
			{
				offsets[count].y = std::stof(token);
				++count;		
			}
			std::getline(fs, hitFrames);
			sstr.clear();
			sstr.str(hitFrames);
			std::vector<char> hFrames;
			while (std::getline(sstr, token, ' ')) // Read each word seperated by spaces
			{
				char num = std::stoi(token);
				hFrames.emplace_back(num);
			}
			std::getline(fs, sounds);
			sstr.clear();
			sstr.str(sounds);
			std::vector<char> soundVec;
			while (std::getline(sstr, token, ' ')) // Read each word seperated by spaces
			{
				char num = std::stoi(token);
				soundVec.emplace_back(num);
			}


			unsigned int size = offsets.size();
			DirectX::XMFLOAT2 cycleTot(0.f, 0.f);
			for (unsigned int i = 0; i < size; i++)
			{
				cycleTot.x += offsets[i].x;
				cycleTot.y += offsets[i].y;
			}

			animRanges.emplace_back(offsets, hFrames, soundVec, cycleTot, name, min, max, animRefresh);
		}
	}

		std::ifstream fs2(dr.spriteAnimFiles[id]);
		std::string line;
		if (fs2)
		{
			while (std::getline(fs2, line)) // Read each line from level file
			{
				if (line == "*")
				{
					while (std::getline(fs2, line))
					{
						std::string name;
						std::vector<int> seq;
						std::vector<float> dur;
						std::vector<DirectX::XMFLOAT2> offsets;

						if (line == "*")
							break;
						line.erase(remove(line.begin(), line.end(), ' '), line.end());
						name = line;

						std::getline(fs2, line);				
						std::istringstream sst(line);

						int temp;
						while (sst >> temp)
						{
							seq.emplace_back(temp);

							if (sst.peek() == ' ')
								sst.ignore();
						}

						float f;
						std::getline(fs2, line);
						sst.clear();
						sst.str(line);
						while (sst >> f)
						{
							dur.emplace_back(f);
							if (sst.peek() == ' ')
								sst.ignore();
						}
						
						std::getline(fs2, line);
						sst.clear();
						sst.str(line);
						DirectX::XMFLOAT2 offset;
						while (sst >> f)
						{
							offset.x = f;
							offsets.push_back(offset);
							if (sst.peek() == ' ')
								sst.ignore();
						}
	
						std::getline(fs2, line);
						sst.clear();
						sst.str(line);
						int count = 0;
						while (sst >> f)
						{
							offsets[count].y = f;
							++count;
							if (sst.peek() == ' ')
								sst.ignore();
						}

						behaviors.emplace_back(offsets, dur, seq, name);
					}
				}
			}
		}
}

int AI::GetBehaviorIndex(std::string b)
{
	int bIndex = 0;
	
	for (; bIndex < behaviors.size(); bIndex++)
	{
		if (behaviors[bIndex].animName == b)
			return bIndex;
	}
	return -1;
}

void AI::UpdateMCAnimation(Sprite& s)
{
	if (s.accumTime > 300000)
		s.accumTime = 0;
	if (s.accumTime > s.animationRefresh)
	{
		if (s.spriteSheetIndex > animRanges[s.animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[s.animRangeIndex].max)
		{
			++s.spriteSheetIndex;
		}
		else
		{
			if(s.spriteSheetIndex == animRanges[s.animRangeIndex].max)
				s.inBattleAnimation = false;
			s.spriteSheetIndex = animRanges[s.animRangeIndex].min;
		}

		s.accumTime = 0;
	}
}

void AI::UpdateMCBAnimation(Sprite& s, float dt)
{
	if (s.accumTime > 300000)
		s.accumTime = 0;
	/*float dltX = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed * dt;
	float dltY = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed * dt;*/
	float dltX = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed;
	float dltY = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed;
	s.origin.x += dltX;
	s.origin.y += dltY;
	s.pos.x += dltX;
	s.pos.y += dltY;
	s.bb.origin.x += dltX;
	s.bb.origin.y += dltY;
	//TODO COLLISION DETECTION
	if (s.accumTime > s.animationRefresh)
	{

		if (s.spriteSheetIndex > animRanges[s.animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[s.animRangeIndex].max)
		{
			++s.spriteSheetIndex;
			++s.animRangeFrameIndex;
		}
		else
		{
			if (s.spriteSheetIndex == animRanges[s.animRangeIndex].max)
				s.inBattleAnimation = false;
			s.spriteSheetIndex = animRanges[s.animRangeIndex].min;
			s.animRangeFrameIndex = 0;
		}

		s.accumTime = 0;
	}
}


void AI::UpdateMCBAnimation(Sprite& s, Camera& c, float dt)
{
	if (s.accumTime > 300000)
		s.accumTime = 0;
	/*float dltX = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed * dt;
	float dltY = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed * dt;*/
	float dltX = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed;
	float dltY = animRanges[s.animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed;
	s.origin.x += dltX;
	s.origin.y += dltY;
	s.pos.x += dltX;
	s.pos.y += dltY;
	s.bb.origin.x += dltX;
	s.bb.origin.y += dltY;

	if (s.origin.x >= c.margins.left && s.origin.x <= c.margins.right) 
	{
		c.pos.x = s.origin.x;
		c.focus.x = s.origin.x;
		
	}
	if (s.origin.y >= c.margins.top && s.origin.y <= c.margins.bottom)
	{
		c.pos.y = s.origin.y;
		c.focus.y = s.origin.y;
	}
	if (s.accumTime > s.animationRefresh)
	{

		if (s.spriteSheetIndex > animRanges[s.animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[s.animRangeIndex].max)
		{
			++s.spriteSheetIndex;
			++s.animRangeFrameIndex;
		}
		else
		{
			if (s.spriteSheetIndex == animRanges[s.animRangeIndex].max)
				s.inBattleAnimation = false;
			s.spriteSheetIndex = animRanges[s.animRangeIndex].min;
			s.animRangeFrameIndex = 0;
		}

		s.accumTime = 0;
	}
}


void AI::UpdateBehaviorAnimation(Sprite& s, std::string behavior, float dt)
{

	if (s.accumTime > s.animationRefresh)
	{
		if (s.accumTime > 300000)
			s.accumTime = 0;
		s.behavior = behavior;
		unsigned int bIndex = 0;

		for (; bIndex < behaviors.size(); bIndex++)
		{
			if (behaviors[bIndex].animName == behavior)
				break;
		}

		if (bIndex == behaviors.size())
			return;
		int animRangeIndex = behaviors[bIndex].animSequence[s.behaviorSeqIndex];
		s.animationRefresh = animRanges[animRangeIndex].animRefresh;
		float dur = behaviors[bIndex].durations[s.behaviorSeqIndex];

		if ((dur > 0 && s.seqAccumTime > dur) ||
			abs(s.accumOffset.x) > abs(behaviors[bIndex].posOffsets[s.behaviorSeqIndex].x) && !s.inBattleAnimation)
		{
			if (s.behaviorSeqIndex == behaviors[bIndex].animSequence.size() - 1)
				s.behaviorSeqIndex = 0;
			else
				++s.behaviorSeqIndex;

			s.accumOffset.x = 0.f;
			s.accumOffset.y = 0.f;
			s.animRangeFrameIndex = 0;
			animRangeIndex = behaviors[bIndex].animSequence[s.behaviorSeqIndex];
			s.animationRefresh = animRanges[animRangeIndex].animRefresh;
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.seqAccumTime = 0;
		}
		/*float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed * dt;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed * dt;*/
		float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed;
		s.origin.x += dltX;
		s.origin.y += dltY;
		s.pos.x += dltX;
		s.pos.y += dltY;
		s.bb.origin.x += dltX;
		s.bb.origin.y += dltY;
		s.accumOffset.x += dltX;
		s.accumOffset.y += dltY;

		if (s.spriteSheetIndex > animRanges[animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[animRangeIndex].max)
		{
			++s.spriteSheetIndex;
			++s.animRangeFrameIndex;
			s.inBattleAnimation = true;
		}
		else
		{
			s.inBattleAnimation = false;
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.animRangeFrameIndex = 0;
		}

		s.accumTime = 0;	
	}
}

void AI::UpdateBehaviorAnimation(Sprite& s, float dt)
{
	if (s.accumTime > s.animationRefresh)
	{
		if (s.accumTime > 300000)
			s.accumTime = 0;
	
		unsigned int bIndex = 0;

		for (; bIndex < behaviors.size(); bIndex++)
		{
			if (behaviors[bIndex].animName == s.behavior)
				break;
		}

		if (bIndex == behaviors.size())
			return;
		int animRangeIndex = behaviors[bIndex].animSequence[s.behaviorSeqIndex];
		s.animationRefresh = animRanges[animRangeIndex].animRefresh;
		float dur = behaviors[bIndex].durations[s.behaviorSeqIndex];

		if ((dur > 0 && s.seqAccumTime > dur) ||
			abs(s.accumOffset.x) > abs(behaviors[bIndex].posOffsets[s.behaviorSeqIndex].x) && !s.inBattleAnimation)
		{
			if (s.behaviorSeqIndex == behaviors[bIndex].animSequence.size() - 1)
				s.behaviorSeqIndex = 0;
			else
				++s.behaviorSeqIndex;

			s.accumOffset.x = 0.f;
			s.accumOffset.y = 0.f;
			s.animRangeFrameIndex = 0;
			animRangeIndex = behaviors[bIndex].animSequence[s.behaviorSeqIndex];
			s.animationRefresh = animRanges[animRangeIndex].animRefresh;
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.seqAccumTime = 0;
		}
		/*float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed * dt;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed * dt;*/
		float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed;
		s.origin.x += dltX;
		s.origin.y += dltY;
		s.pos.x += dltX;
		s.pos.y += dltY;
		s.bb.origin.x += dltX;
		s.bb.origin.y += dltY;
		s.accumOffset.x += dltX;
		s.accumOffset.y += dltY;

		if (s.spriteSheetIndex > animRanges[animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[animRangeIndex].max)
		{
			++s.spriteSheetIndex;
			++s.animRangeFrameIndex;
		}
		else
		{
			s.inBattleAnimation = false;
			s.spriteSheetIndex = animRanges[animRangeIndex].min; // could set spritesheet index to idle? or walk?
			s.animRangeFrameIndex = 0;
		}

		s.accumTime = 0;
	}
}

void AI::PlayBattleSequenceAnimation(Sprite & s, Sprite& target, std::vector<BattleCommand> commands, float dt, std::unique_ptr<DirectX::WaveBank>const &wb, std::unique_ptr<DirectX::SoundEffectInstance> &sound)
{
	if (s.accumTime > s.animationRefresh && s.inBattleAnimation)
	{
		if (s.accumTime > 300000)
			s.accumTime = 0;

		int animRangeIndex = 0;
		for (; animRangeIndex < animRanges.size() - 1; animRangeIndex++)
		{
			if (commands[s.commandIndex].name == animRanges[animRangeIndex].name)
				break;
		}
		if (animRangeIndex == animRanges.size())
		{
			//command not found in animRange of sprite. Set back to Idle;
			return;
		}
		else
			s.animRangeIndex = animRangeIndex;

		if (s.animName != commands[s.commandIndex].name)
		{
			s.animName = commands[s.commandIndex].name;
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
		}

		s.animationRefresh = animRanges[animRangeIndex].animRefresh;

		if (s.spriteSheetIndex > animRanges[animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[animRangeIndex].max)
		{
			++s.spriteSheetIndex;
			++s.animRangeFrameIndex;
			if (animRanges[animRangeIndex].hitFrames[s.animRangeFrameIndex])
			{
				target.hitStatus = animRanges[animRangeIndex].hitFrames[s.animRangeFrameIndex];
				target.animName = "Idle" + target.orientation;
				if (s.soundAccumTime > .65f || animRanges[animRangeIndex].soundIndices[0] == SOUNDS_SWISHHPUNCH)
				{
					int max = animRanges[animRangeIndex].soundIndices.size() - 1;
					std::random_device rd; // obtain a random number from hardware
					std::mt19937 eng(rd()); // seed the generator
					std::uniform_int_distribution<> distr(0, max); // define the range
					int soundIdx = animRanges[animRangeIndex].soundIndices[distr(eng)];
					if (soundIdx > -1)
					{
						sound = wb->CreateInstance(soundIdx);
						sound->SetVolume(2.f);
						sound->Play(); // play the attack grunt sound
						s.soundAccumTime = 0;
					}
				}
			}
		}
		else
		{		
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.animRangeFrameIndex = 0;
			++s.cycleCount;
			if (s.cycleCount == commands[s.commandIndex].cycleCount)
			{
				++s.commandIndex;
				s.cycleCount = 0;
				if (s.commandIndex == commands.size())
				{
					s.commandIndex = 0;
					s.inBattleAnimation = false;
				}
			}
		}

		/*float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed * dt;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed * dt;*/
		float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed;
		if ( abs(commands[s.commandIndex].distance.y) > 0.f )
		{
			int temp = ceil ((commands[s.commandIndex].distance.x / (animRanges[animRangeIndex].cycleOffset.x * s.speed)) * animRanges[animRangeIndex].posOffsets.size());
			dltY = commands[s.commandIndex].distance.y / temp;
		}
		s.accumOffset.x += dltX;
		s.accumOffset.y += dltY;
		s.origin.x += dltX;
		s.origin.y += dltY;

		if (commands[s.commandIndex].cycleCount == 0 && abs(s.accumOffset.x) >= abs(commands[s.commandIndex].distance.x))
		{
			s.origin.x -= (s.accumOffset.x - commands[s.commandIndex].distance.x);
			s.origin.y += (commands[s.commandIndex].distance.y - s.accumOffset.y);
			s.accumOffset.x = 0.f;
			s.accumOffset.y = 0.f;
			s.animRangeFrameIndex = 0;
			++s.commandIndex;
			s.cycleCount = 0;
			if (s.commandIndex == commands.size())
			{
				s.commandIndex = 0;
				s.inBattleAnimation = false;
			}
		}
		s.accumTime = 0;
	}
}

void AI::LoopConditionAnimation(Sprite & s, float dt)
{
	if (s.accumTime > s.animationRefresh)
	{
		if (s.accumTime > 300000)
			s.accumTime = 0;

		std::string condStr;
		switch (s.condition)
		{
		case 0:
			condStr = "Idle";
			break;
		case 1: 
			condStr = "Tired";
			break;
		case 2:
			condStr = "Wounded";
			break;
		case 3: 
			condStr = "Critical";
			break;
		case 4:
			condStr = "KO";
		default:
			break;
		}
		switch (s.orientation)
		{
		case 'L':
			condStr += "L";
			break;
		case 'R':
			condStr += "R";
			break;
		default:
			break;
		}
		
		int animRangeIndex = 0;
		for (; animRangeIndex < animRanges.size() - 1; animRangeIndex++)
		{
			if (condStr == animRanges[animRangeIndex].name)
				break;
		}
		if (animRangeIndex == animRanges.size())
		{
			//command not found in animRange of sprite. Set back to Idle;
			return;
		}
		else
			s.animRangeIndex = animRangeIndex;
		if (s.animName != condStr)
		{
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.animName = condStr;
		}
		s.animationRefresh = animRanges[animRangeIndex].animRefresh;

		if (s.spriteSheetIndex > animRanges[animRangeIndex].min - 1 && s.spriteSheetIndex < animRanges[animRangeIndex].max)
		{
			++s.spriteSheetIndex;
			++s.animRangeFrameIndex;
		}
		else
		{
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.animRangeFrameIndex = 0;
		}
		s.accumTime = 0;
	}
}

void AI::PlayHitStatus(Sprite & s, float dt, std::unique_ptr<DirectX::WaveBank>const &wb, std::unique_ptr<DirectX::SoundEffectInstance> &painSound, std::unique_ptr<DirectX::SoundEffectInstance> &powSound)
{
	if (s.accumTime > s.animationRefresh)
	{
		s.accumTime = 0;

		std::string statusStr;
		switch (s.hitStatus)
		{
		case 1:
			statusStr = "HitH";
			break;
		case 2:
			statusStr = "HitM";
			break;
		case 3:
			statusStr = "KO";
			break;
		case 4:
			statusStr = "DEFEATED";
			break;
		default:
			break;
		}
		switch (s.orientation)
		{
		case 'L':
			statusStr += "L";
			break;
		case 'R':
			statusStr += "R";
			break;
		default:
			break;
		}

		int animRangeIndex = 0;
		for (; animRangeIndex < animRanges.size() - 1; animRangeIndex++)
		{
			if (statusStr == animRanges[animRangeIndex].name)
				break;
		}
		if (animRangeIndex == animRanges.size())
		{
			//command not found in animRange of sprite. Set back to Idle;
			return;
		}
		else
			s.animRangeIndex = animRangeIndex;

		++s.spriteSheetIndex;
		++s.animRangeFrameIndex;

		if (s.animName != statusStr)
		{
			s.hitPoints -= 10;
			s.spriteSheetIndex = animRanges[animRangeIndex].min;
			s.animationRefresh = animRanges[animRangeIndex].animRefresh;
			s.animRangeIndex = animRangeIndex;
			s.animRangeFrameIndex = 0;
			s.animName = statusStr;
			std::random_device rd; // obtain a random number from hardware
			std::mt19937 eng(rd()); // seed the generator
			if (s.soundAccumTime > 2.f || s.hitStatus == HIT_MID)
			{
				int max = animRanges[animRangeIndex].soundIndices.size() - 1;
				std::uniform_int_distribution<> distr(0, max); // define the range
				int random = distr(eng);
				int soundIdx = animRanges[animRangeIndex].soundIndices[random];
				if (soundIdx > -1)
				{
					painSound = wb->CreateInstance(soundIdx);
					painSound->Play(); // play scream sounds
					s.soundAccumTime = 0;
				}
			}
			
			std::uniform_int_distribution<> dist(SOUNDS_HITCRUNCH1, SOUNDS_HITTHUD2); 
			powSound = wb->CreateInstance(dist(eng));
			powSound->Play(); // play hit sounds
		}

		if (s.spriteSheetIndex > animRanges[animRangeIndex].max)
		{
			if (s.hitPoints > 0)
				s.hitStatus = NONE;
			else
			{
				s.hitStatus = KO;
				s.condition = DEAD;
			}
			s.animRangeFrameIndex = 0;
			s.spriteSheetIndex = animRanges[animRangeIndex].max;
			return;
		}

		float dltX = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].x * s.speed;
		float dltY = animRanges[animRangeIndex].posOffsets[s.animRangeFrameIndex].y * s.speed;
		s.origin.x += dltX;
		s.origin.y += dltY;
	}
}

