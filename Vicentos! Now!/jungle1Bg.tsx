<?xml version="1.0" encoding="UTF-8"?>
<tileset name="jungle1" tilewidth="640" tileheight="480" tilecount="48" columns="6">
 <image source="../../../../Documents/jungle1.png" width="3852" height="3856"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="190" width="640" height="20"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="302" y="0" width="20" height="480"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="226" width="640" height="20"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="340" y="0" width="20" height="480"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="302" y="204" width="20" height="276"/>
   <object id="2" x="312" y="190" width="328" height="20"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="0" y="190" width="370" height="20"/>
   <object id="2" x="340" y="196" width="20" height="284"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index">
   <object id="1" x="302" y="0" width="20" height="274"/>
   <object id="2" x="304" y="226" width="336" height="20"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="0" y="226" width="372" height="20"/>
   <object id="2" x="340" y="2" width="20" height="218"/>
  </objectgroup>
 </tile>
</tileset>
