#pragma once

class SSFactory
{
public:
	std::vector<SpriteSheet> spriteSheets;
	
	int GetIndex(int ssID);
	bool Contains(int ssID);
	void Clear();
	void UpUnitCount(int ssID, int numUnits);
	void AddSS(int ssID, DataResource& dr, ID3D11Device1* dev);
};
