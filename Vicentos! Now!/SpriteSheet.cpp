#include "pch.h"
#include "SpriteSheet.h"

SpriteSheet::SpriteSheet()
{
	mTexture = nullptr;
	ssType = 0;
	unitCount = 0;
	id = -1;
	speed = 0.f;
	scale = 0.f;
	animRefresh = 0.f;
}

SpriteSheet::~SpriteSheet()
{
	mTexture.Reset();
}

void SpriteSheet::Load(int i, ID3D11Device1* dev, DataResource& dr)
{
	id = i;
	ssFrames.clear();
	mTexture.Reset();
	unitCount = 0;
	unitSize.x = 0.f;
	unitSize.y = 0.f;

	std::wifstream inFile;
	std::ifstream in;
	switch (ssType)
	{
		case 1:
		{
			DX::ThrowIfFailed(DirectX::CreateDDSTextureFromFile(dev, dr.spriteDDSFiles[id], nullptr, mTexture.ReleaseAndGetAddressOf()));
			inFile.open(dr.spriteTxtFiles[id]);
			in.open(dr.spriteTxtFiles[id]);
			string temp, val;
			int count = 0;
			std::getline(in, temp);
			temp.erase(0, 2);
			std::stringstream sstr(temp);
			while (getline(sstr, val, ' '))
			{
				if (count == 0)
					speed = std::stof(val);
				else if (count == 1)
					animRefresh = std::stof(val);
				else if (count == 2)
					scale = std::stof(val);
				++count;
			}

			break;
		}
		case 2:
		{
			DX::ThrowIfFailed(DirectX::CreateDDSTextureFromFile(dev, dr.mapBgDDSFiles[id], nullptr, mTexture.ReleaseAndGetAddressOf()));
			inFile.open(dr.mapBgTxtFiles[id]);
			break;
		}
		case 3:
		{
			inFile.open(dr.mapFgTxtFiles[id]);
			if (!inFile)
				return;
			else
				DX::ThrowIfFailed(DirectX::CreateDDSTextureFromFile(dev, dr.mapFgDDSFiles[id], nullptr, mTexture.ReleaseAndGetAddressOf()));
			break;
		}
		default:
			break;
	}

	wchar_t strLine[1024];

	for (;;)
	{
		inFile >> strLine;
		if (!inFile)
			break;

		if (0 == wcscmp(strLine, L"#"))
		{
			// Comment
		}
		else
		{
			// Parse lines of form: Name;rotatedInt;xInt;yInt;widthInt;heightInt;origWidthInt;origHeightInt;offsetXFloat;offsetYFloat
			static const wchar_t* delim = L";\n";

			wchar_t* context = nullptr;
			wchar_t* name = wcstok_s(strLine, delim, &context);
			if (!name || !*name)
				throw std::exception("SpriteSheet encountered invalid .txt data");

			//if ( mSprites.find( name ) != mSprites.cend() )
				//throw std::exception("SpriteSheet encountered duplicate in .txt data");

			wchar_t* str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");

			SpriteFrame frame;
			frame.rotated = (_wtoi(str) == 1);

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			frame.sourceRect.left = _wtol(str);

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			frame.sourceRect.top = _wtol(str);

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			LONG dx = _wtol(str);
			frame.sourceRect.right = frame.sourceRect.left + dx;

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			LONG dy = +_wtol(str);
			frame.sourceRect.bottom = frame.sourceRect.top + dy;

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			frame.size.x = static_cast<float>(_wtof(str));

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			frame.size.y = static_cast<float>(_wtof(str));

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			float pivotX = static_cast<float>(_wtof(str));

			str = wcstok_s(nullptr, delim, &context);
			if (!str)
				throw std::exception("SpriteSheet encountered invalid .txt data");
			float pivotY = static_cast<float>(_wtof(str));

			if (frame.rotated)
			{
				frame.origin.x = dx * (1.f - pivotY);
				frame.origin.y = dy * pivotX;
			}
			else
			{
				frame.origin.x = (dx * pivotX);
				frame.origin.y = (dy * pivotY);
			}
			ssFrames.emplace_back(frame);
		}

		inFile.ignore(1000, '\n');
	}

	unitSize.x = ssFrames[0].size.x;
	unitSize.y = ssFrames[0].size.y;
}

Sprite SpriteSheet::CreateSprite(DirectX::XMFLOAT2 org, bool col, std::string beh, unsigned char o, float sc)
{
	Sprite sprite;
	sprite.id = id;
	sprite.scale = scale * sc;
	sprite.size.x = unitSize.x * scale * sc;
	sprite.size.y = unitSize.y * scale * sc;

	sprite.origin.x = org.x;
	sprite.origin.y = org.y;
	sprite.pos.x = sprite.origin.x - sprite.size.x / 2.f;
	sprite.pos.y = sprite.origin.y - sprite.size.y / 2.f;


	sprite.animationRefresh = animRefresh;
	sprite.speed = speed;
	sprite.spriteSheetIndex = 0;

	sprite.bb.origin.x = sprite.origin.x;
	sprite.bb.origin.y = sprite.origin.y;
	//sprite.bb.origin.y = sprite.origin.y  + (sprite.size.y * .375f);
	//sprite.bb.size.y = sprite.size.y * .25f;
	sprite.bb.size.x = sprite.size.x;
	sprite.bb.size.y = sprite.size.y;

	sprite.bb.collideable = col;
	sprite.accumTime = 0.f;
	sprite.seqAccumTime = 0.f;
	sprite.accumOffset.x = 0.f;
	sprite.accumOffset.y = 0.f;
	sprite.animRangeIndex = 0;
	sprite.behaviorSeqIndex = 0;
	sprite.behavior = beh;
	if (beh == "IdleL")
		sprite.animName = beh;
	else if (beh == "IdleR")
		sprite.animName = beh;
	else
		sprite.animName = "";
	sprite.hitStatus = 0;
	sprite.condition = 0;
	sprite.soundIndex = -1;
	sprite.commandIndex = 0;
	sprite.cycleCount = 0;
	sprite.hitPoints = 120;
	sprite.animRangeFrameIndex = 0;
	sprite.orientation = o;
	++unitCount;

	return sprite;
}

// Draw overloads specifying position and scale as XMFLOAT2.
void XM_CALLCONV SpriteSheet::Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::XMFLOAT2 const& position,
	DirectX::FXMVECTOR color, float rotation, float scale, DirectX::SpriteEffects effects, float layerDepth) const
{
	assert(batch != 0);
	using namespace DirectX;

	if (frame.rotated)
	{
		rotation -= XM_PIDIV2;
		switch (effects)
		{
		case SpriteEffects_FlipHorizontally:    effects = SpriteEffects_FlipVertically; break;
		case SpriteEffects_FlipVertically:      effects = SpriteEffects_FlipHorizontally; break;
		}
	}

	XMFLOAT2 origin = frame.origin;
	switch (effects)
	{
	case SpriteEffects_FlipHorizontally:    origin.x = frame.sourceRect.right - frame.sourceRect.left - origin.x; break;
	case SpriteEffects_FlipVertically:      origin.y = frame.sourceRect.bottom - frame.sourceRect.top - origin.y; break;
	}

	batch->Draw(mTexture.Get(), position, &frame.sourceRect, color, rotation, origin, scale, effects, layerDepth);
}

void XM_CALLCONV SpriteSheet::Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::XMFLOAT2 const& position,
	DirectX::FXMVECTOR color, float rotation, DirectX::XMFLOAT2 const& scale, DirectX::SpriteEffects effects, float layerDepth) const

{
	assert(batch != 0);
	using namespace DirectX;

	if (frame.rotated)
	{
		rotation -= XM_PIDIV2;
		switch (effects)
		{
		case SpriteEffects_FlipHorizontally:    effects = SpriteEffects_FlipVertically; break;
		case SpriteEffects_FlipVertically:      effects = SpriteEffects_FlipHorizontally; break;
		}
	}

	XMFLOAT2 origin = frame.origin;
	switch (effects)
	{
	case SpriteEffects_FlipHorizontally:    origin.x = frame.sourceRect.right - frame.sourceRect.left - origin.x; break;
	case SpriteEffects_FlipVertically:      origin.y = frame.sourceRect.bottom - frame.sourceRect.top - origin.y; break;
	}

	batch->Draw(mTexture.Get(), position, &frame.sourceRect, color, rotation, origin, scale, effects, layerDepth);
}

// Draw overloads specifying position and scale via the first two components of an XMVECTOR.
void XM_CALLCONV SpriteSheet::Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::FXMVECTOR position,
	DirectX::FXMVECTOR color, float rotation, float scale, DirectX::SpriteEffects effects, float layerDepth) const
{
	assert(batch != 0);
	using namespace DirectX;

	if (frame.rotated)
	{
		rotation -= XM_PIDIV2;
		switch (effects)
		{
		case SpriteEffects_FlipHorizontally:    effects = SpriteEffects_FlipVertically; break;
		case SpriteEffects_FlipVertically:      effects = SpriteEffects_FlipHorizontally; break;
		}
	}

	XMFLOAT2 origin = frame.origin;
	switch (effects)
	{
	case SpriteEffects_FlipHorizontally:    origin.x = frame.sourceRect.right - frame.sourceRect.left - origin.x; break;
	case SpriteEffects_FlipVertically:      origin.y = frame.sourceRect.bottom - frame.sourceRect.top - origin.y; break;
	}
	XMVECTOR vorigin = XMLoadFloat2(&origin);

	batch->Draw(mTexture.Get(), position, &frame.sourceRect, color, rotation, vorigin, scale, effects, layerDepth);
}

void XM_CALLCONV SpriteSheet::Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::FXMVECTOR position,
	DirectX::FXMVECTOR color, float rotation, DirectX::GXMVECTOR scale, DirectX::SpriteEffects effects, float layerDepth) const
{
	assert(batch != 0);
	using namespace DirectX;

	if (frame.rotated)
	{
		rotation -= XM_PIDIV2;
		switch (effects)
		{
		case SpriteEffects_FlipHorizontally:    effects = SpriteEffects_FlipVertically; break;
		case SpriteEffects_FlipVertically:      effects = SpriteEffects_FlipHorizontally; break;
		}
	}

	XMFLOAT2 origin = frame.origin;
	switch (effects)
	{
	case SpriteEffects_FlipHorizontally:    origin.x = frame.sourceRect.right - frame.sourceRect.left - origin.x; break;
	case SpriteEffects_FlipVertically:      origin.y = frame.sourceRect.bottom - frame.sourceRect.top - origin.y; break;
	}
	XMVECTOR vorigin = XMLoadFloat2(&origin);

	batch->Draw(mTexture.Get(), position, &frame.sourceRect, color, rotation, vorigin, scale, effects, layerDepth);
}

// Draw overloads specifying position as a RECT.
void XM_CALLCONV SpriteSheet::Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, RECT const& destinationRectangle,
	DirectX::FXMVECTOR color, float rotation, DirectX::SpriteEffects effects, float layerDepth) const
{
	assert(batch != 0);
	using namespace DirectX;

	if (frame.rotated)
	{
		rotation -= XM_PIDIV2;
		switch (effects)
		{
		case SpriteEffects_FlipHorizontally:    effects = SpriteEffects_FlipVertically; break;
		case SpriteEffects_FlipVertically:      effects = SpriteEffects_FlipHorizontally; break;
		}
	}

	XMFLOAT2 origin = frame.origin;
	switch (effects)
	{
	case SpriteEffects_FlipHorizontally:    origin.x = frame.sourceRect.right - frame.sourceRect.left - origin.x; break;
	case SpriteEffects_FlipVertically:      origin.y = frame.sourceRect.bottom - frame.sourceRect.top - origin.y; break;
	}

	batch->Draw(mTexture.Get(), destinationRectangle, &frame.sourceRect, color, rotation, origin, effects, layerDepth);
}

/*
const SpriteFrame* Find(const wchar_t* name) const
{
	auto it = mSprites.find(name);
	if (it == mSprites.cend())
		return nullptr;

	return &it->second;
}*/

/*const SpriteFrame* Find(int index) const
{
	auto it = mSpritesIndexed.find(index);
	if (it == mSpritesIndexed.cend())
		return nullptr;

	return &it->second;
}*/