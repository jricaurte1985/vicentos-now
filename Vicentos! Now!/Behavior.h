#pragma once

class Behavior
{
public:
	std::vector<DirectX::XMFLOAT2> posOffsets;
	std::vector<float> durations;
	std::string animName;
	std::vector<int> animSequence;

	Behavior();
	Behavior(std::vector<DirectX::XMFLOAT2> offsets, std::vector<float> dur, std::vector<int> seq, std::string n);
	Behavior(const Behavior& obj);
};