#pragma once

class ScreenTransition
{
public:
	bool active;
	bool fadeIn;
	int frameCountBlack;
	float alphaDelta;
	float alphaValue;
	std::unique_ptr<DirectX::CommonStates> m_states;
	std::unique_ptr<DirectX::BasicEffect> m_effect;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> m_inputLayout;

	ScreenTransition();
	~ScreenTransition();
	void Init(ID3D11Device1* dev);
	void Render(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* pb, ID3D11DeviceContext1* context);
};

