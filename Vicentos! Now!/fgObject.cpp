#include "pch.h"
#include "FgObject.h"


FgObject::FgObject()
{
}

FgObject::FgObject(DirectX::XMFLOAT2 sz, DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 p, bool col, int i, int ssIdx)
	: size(sz), origin(org), pos(p), collideable(col), id(i), ssIndex(ssIdx)
{
}

FgObject::~FgObject()
{
}
