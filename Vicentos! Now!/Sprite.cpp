#include "pch.h"
#include "Sprite.h"
using namespace DirectX;

Sprite::Sprite() 
{
	id = 0;
	scale = 0.f;
	size.x = 0.f;
	size.y = 0.f;

	origin.x = 0.f;
	origin.y = 0.f;
	pos.x = 0.f;
	pos.y = 0.f;


	animationRefresh = 0.f;
	speed = 0.f;
	spriteSheetIndex = 0;
	behaviorSeqIndex = 0;
	accumTime = 0.f;
	accumOffset.x = 0.f;
	accumOffset.y = 0.f;
	soundAccumTime = 0.f;
	seqAccumTime = 0.f;
	animRangeIndex = 0;
	animRangeFrameIndex = 0;
	commandIndex = 0;
	cycleCount = 0;
	inBattleAnimation = false;
	soundBit = false;
	orientation = 'R';
	condition = 0;
	hitStatus = 0;
	soundIndex = -1;
	hitPoints = 120;

	bb.origin.x = 0.f;
	bb.origin.y = 0.f;
	bb.size.x = 0.f;
	bb.size.y = 0.f;
	bb.collideable = false;
	bb.id = -1;
	bb.parentTileId = -1;
	bb.ssIndex = -1;
};

Sprite::Sprite(int i, DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 p, DirectX::XMFLOAT2 s, BB b, float sp, float animR, std::string beh, std::string aName, float sc = 1.f, unsigned char o = 'r')
	: id(i), origin(org), pos(p), size(s), bb(b), speed(sp), animationRefresh(animR), scale(sc), behavior(beh), animName(aName), orientation(o)
{
	
}

Sprite::Sprite(const Sprite& obj)
{
	id = obj.id;
	scale = obj.scale;
	size.x = obj.size.x;
	size.y = obj.size.y;

	origin.x = obj.origin.x;
	origin.y = obj.origin.y;
	pos.x = obj.origin.x - obj.size.x / 2.f;
	pos.y = obj.origin.y - obj.size.y / 2.f;


	animationRefresh = obj.animationRefresh;
	speed = obj.speed;
	spriteSheetIndex = obj.spriteSheetIndex;
	behaviorSeqIndex = 0;
	accumTime = 0.f;
	seqAccumTime = 0.f;
	accumOffset.x = 0.f;
	accumOffset.y = 0.f;
	soundAccumTime = 0.f;
	soundBit = obj.soundBit;
	animName = obj.animName;
	animRangeIndex = obj.animRangeIndex;
	behavior = obj.behavior;
	inBattleAnimation = obj.inBattleAnimation;
	animRangeFrameIndex = obj.animRangeFrameIndex;
	orientation = obj.orientation;
	condition = 0;
	commandIndex = obj.commandIndex;
	cycleCount = obj.cycleCount;
	hitStatus = obj.hitStatus;
	soundIndex = obj.soundIndex;
	hitPoints = obj.hitPoints;

	bb.origin.x = obj.bb.origin.x;
	bb.origin.y = obj.bb.origin.y;
	bb.size.x = obj.bb.size.x;
	bb.size.y = obj.bb.size.y;
	bb.collideable = obj.bb.collideable;
	bb.id = obj.bb.id;
	bb.parentTileId = obj.bb.parentTileId;
	bb.collideable = obj.bb.collideable;
	bb.ssIndex = obj.bb.ssIndex;
	
}

void Sprite::Place(DirectX::XMFLOAT2 p)
{
	origin.x = p.x;
	origin.y = p.y;
	pos.x = origin.x - size.x / 2.f;
	pos.y = origin.y - size.y / 2.f;
	bb.origin.x = origin.x;
	bb.origin.y = origin.y;
	bb.size.x = size.x;
	bb.size.y = size.y;
	//bb.origin.y = origin.y + (size.y * .375f);
	//bb.size.y = size.y * .25f;
}

void Sprite::SetBehavior(std::string b)
{
	behavior = b;
	behaviorSeqIndex = 0;
}








/* code for wchar stuff

	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, n.c_str(), -1, NULL, 0);
	wchar_t* wName = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, n.c_str(), -1, wName, wchars_num);

	std::wstring metadata(wName);
	bool test = false;
	metadata += L".txt";
	delete[] wName;
	*/