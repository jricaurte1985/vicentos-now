#pragma once
class BattleCommand
{
public:
	std::string name;
	DirectX::XMFLOAT2 distance;
	int cycleCount;
	BattleCommand();
	BattleCommand(std::string n, int c, DirectX::XMFLOAT2 dis);
	BattleCommand(std::string n, int c);
	BattleCommand(std::string n, DirectX::XMFLOAT2 dis);
	~BattleCommand();
};

