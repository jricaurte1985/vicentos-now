#include "pch.h"
#include "BB.h"

BB::BB()
{
	origin.x = 0.f;
	origin.y = 0.f;
	size.x = 0.f;
	size.y = 0.f;
	collideable = false;
}

BB::BB(DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 sz, int i, int tId, int sIdx, bool c) :
	origin(org), size(sz), id(i), parentTileId(tId), ssIndex(sIdx), collideable(c) {}
