#pragma once

#include <DirectXMath.h>
#include "SimpleMath.h"

class Camera
{
public:
	DirectX::SimpleMath::Matrix world;
	DirectX::SimpleMath::Matrix view;
	DirectX::SimpleMath::Matrix proj;
	DirectX::SimpleMath::Matrix wvp;

	DirectX::SimpleMath::Vector3 pos;
	DirectX::SimpleMath::Vector3 focus;

	RECT margins;
	float camWidth, camHeight, mapWidth, mapHeight, speed;
	bool topLocked, bottomLocked, leftLocked, rightLocked;

	Camera();
	Camera(float, float, float, float, RECT, DirectX::SimpleMath::Vector3, DirectX::SimpleMath::Vector3, float, bool, bool, bool, bool);

	void SetLocks();
	void UpdateMargins(float cWidth, float cHeight, float mapW, float mapH);
	void OnWindowSizeChange(float w, float h, float mapW, float mapH);
	void OnWindowSizeChange(float cW, float cH, float offset);
	void InitializeCamera(float, float, float, float, DirectX::SimpleMath::Vector3, DirectX::SimpleMath::Vector3, float);
	void UpdateCamera();
};
