#include "pch.h"
#include "BattleCommand.h"


BattleCommand::BattleCommand()
{
}

BattleCommand::BattleCommand(std::string n, int c, DirectX::XMFLOAT2 dis)
	: name(n), cycleCount(c), distance(dis)
{
}

BattleCommand::BattleCommand(std::string n, int c)
	: name(n), cycleCount(c), distance(0.f, 0.f)
{
}

BattleCommand::BattleCommand(std::string n, DirectX::XMFLOAT2 dis)
	: name(n), distance(dis), cycleCount(0)
{
}


BattleCommand::~BattleCommand()
{
}
