#pragma once

class TransitionZone
{
public:
	BB bb;
	DirectX::XMFLOAT2 newSpawn;
	int drIndex;

	TransitionZone();
	TransitionZone(DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 sz, bool c, int i, DirectX::XMFLOAT2 spawn);
	~TransitionZone();
};

class TransitionLayer
{
public:
	std::vector<TransitionZone> zoneLines;
	void Load(int id, DataResource& dr);
};
