#pragma once
#include "AnimationRange.h"
using namespace std;

class DataResource
{
public:
	const wchar_t* mapBgTxtFiles[3];
	const wchar_t* mapFgTxtFiles[3];
	const char* mapTMXFiles[3];
	const char* mapFgTSXFiles[3];
	const char* mapBgTSXFiles[3];
	const char* mapZLTSXFiles[3];
	const wchar_t* mapBgDDSFiles[3];
	const wchar_t* mapFgDDSFiles[3];
	const wchar_t* battleBgDDSFiles[1];

	const wchar_t* spriteTxtFiles[3];
	const wchar_t* spriteDDSFiles[3];
	const wchar_t* spritePNGFiles[3];
	const char* spriteAnimFiles[3];

	DataResource();
};




