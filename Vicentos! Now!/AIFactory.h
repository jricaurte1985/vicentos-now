#pragma once
#include "AI.h"

class AIFactory
{
public:
	std::vector<AI> AIvec;

	AIFactory();
	~AIFactory();
	bool Contains(int ssID);
	void Clear();
	void AddAI(int aiID, DataResource& dr);
	float GetAnimationRefresh(int spriteId, int animRangeIndex);

};

