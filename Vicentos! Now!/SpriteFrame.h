#pragma once
struct SpriteFrame
{
	RECT                sourceRect;
	DirectX::XMFLOAT2   size;
	DirectX::XMFLOAT2   origin;
	bool                rotated;
};