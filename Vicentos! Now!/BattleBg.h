#pragma once
class BattleBg
{
public:
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mTexture;
	DirectX::XMFLOAT2 size, origin, LeftPos1, LeftPos2, LeftPos3, RightPos1, RightPos2, RightPos3;
	int id;

	BattleBg();
	~BattleBg();
	void Load(int i, DataResource dr, ID3D11Device1* dev);
};

