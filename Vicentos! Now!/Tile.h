#pragma once

struct Tile
{
	DirectX::XMFLOAT2 origin, pos;
	std::vector<int> bbIDs;
	int id, ssIndex;
	
	Tile();
	Tile(DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 p, std::vector<int> bbs, int i, int ssI);
};
