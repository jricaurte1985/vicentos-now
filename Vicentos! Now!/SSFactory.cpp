#include "pch.h"
#include "SSFactory.h"

int SSFactory::GetIndex(int ssID)
{
	int s = spriteSheets.size();
	for (int i = 0; i < s; i++)
	{
		if (spriteSheets[i].id == ssID)
			return i;
	}
	return -1;
}

bool SSFactory::Contains(int ssID)
{
	int s = spriteSheets.size();
	for (int i = 0; i < s; i++)
	{
		if (spriteSheets[i].id == ssID)
			return true;
	}
	return false;
}

void SSFactory::UpUnitCount(int ssID, int numUnits)
{
	int s = spriteSheets.size();
	for (int i = 0; i < s; i++)
	{
		if (spriteSheets[i].id == ssID)
		{
			spriteSheets[i].unitCount+= numUnits;
			return;
		}
	}
}

void SSFactory::AddSS(int ssID, DataResource& dr, ID3D11Device1* dev)
{
	if (!Contains(ssID))
	{
		SpriteSheet ss;
		ss.ssType = 1;
		ss.Load(ssID, dev, dr);
		spriteSheets.emplace_back(ss);
	}
}

void SSFactory::Clear()
{
	spriteSheets.clear();
}