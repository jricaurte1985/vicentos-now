#include "pch.h"
#include "DataResource.h"

DataResource::DataResource()
{
	mapTMXFiles[0] = "Level0.tmx";
	mapTMXFiles[1] = "jungle1.tmx";
	mapTMXFiles[2] = "desert1.tmx";

	mapBgDDSFiles[0] = L"Level0Bg.DDS";
	mapBgDDSFiles[1] = L"jungle1Bg.DDS";
	mapBgDDSFiles[2] = L"desert1Bg.DDS";

	mapBgTxtFiles[0] = L"Level0Bg.txt";
	mapBgTxtFiles[1] = L"jungle1Bg.txt";
	mapBgTxtFiles[2] = L"desert1Bg.txt";

	mapBgTSXFiles[0] = "Level0Bg.tsx";
	mapBgTSXFiles[1] = "jungle1Bg.tsx";
	mapBgTSXFiles[2] = "desert1Bg.tsx";

	mapFgDDSFiles[0] = L"Level0Fg.DDS";
	mapFgDDSFiles[1] = L"Level0Fg.DDS";
	mapFgDDSFiles[2] = L"Level0Fg.DDS";

	mapFgTxtFiles[0] = L"Level0Fg.txt";
	mapFgTxtFiles[1] = L"jungle1Fg.txt";
	mapFgTxtFiles[2] = L"desert1Fg.txt";

	mapFgTSXFiles[0] = "Level0Fg.tsx";
	mapFgTSXFiles[1] = "jungle1Fg.tsx";
	mapFgTSXFiles[2] = "desert1Fg.tsx";

	battleBgDDSFiles[0] = L"Level0Bat1.DDS";


	//spriteTxtFiles[0] = L"Ghost.txt";
	spriteTxtFiles[0] = L"vicentos.txt";
	spriteTxtFiles[1] = L"sig.txt";
	spriteTxtFiles[2] = L"ryu.txt";

	//spriteDDSFiles[0] = L"Ghost.DDS";
	spriteDDSFiles[0] = L"vicentos.DDS";
	spriteDDSFiles[1] = L"sig.DDS";
	spriteDDSFiles[2] = L"ryu.DDS";
	
	//spriteAnimFiles[0] = "ghostAnim.txt";
	spriteAnimFiles[0] = "vicentosAnim.txt";
	spriteAnimFiles[1] = "sigAnim.txt";
	spriteAnimFiles[2] = "ryuAnim.txt";
}