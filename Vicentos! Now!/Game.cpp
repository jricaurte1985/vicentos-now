//
// Game.cpp
//

#include "pch.h"
#include "Game.h"

extern void ExitGame();

using namespace DirectX;

using Microsoft::WRL::ComPtr;

Game::Game() noexcept :
    m_window(nullptr),
    m_outputWidth(1600),
    m_outputHeight(900),
    m_featureLevel(D3D_FEATURE_LEVEL_9_1)
{
	m_mainCharacter.id = 0;
	m_bgLayer.id = 0;
}

Game::~Game()
{
	if (m_audEngine)
	{
		m_audEngine->Suspend();
	}
	m_musicLoop.reset();
}

// Initialize the Direct3D resources required to run.
void Game::Initialize(HWND window, int width, int height)
{
    m_window = window;
    m_outputWidth = std::max(width, 1);
    m_outputHeight = std::max(height, 1);

    CreateDevice();

    CreateResources();

	m_gameState = FREEROAM;
	tlIndex = -1;
	enemyIndex = -1;
	target = 0;
	m_keyboard = std::make_unique<Keyboard>();
	m_mouse = std::make_unique<Mouse>();
	m_mouse->SetWindow(window);
	m_playerBattleActions.reserve(10);
	m_playerBattleActions.emplace_back("IdleR", 0);
	m_playerBattleActions.emplace_back("IdleR", 0);
	m_playerBattleActions.emplace_back("IdleR", 0);
	m_npcBattleActions.reserve(10);
	m_npcBattleActions.emplace_back("IdleL", 0);
	m_npcBattleActions.emplace_back("IdleL", 0);
	m_npcBattleActions.emplace_back("IdleL", 0);
	m_battleScale = 1.3f;

	m_camera.InitializeCamera(static_cast<float>(m_outputWidth), static_cast<float>(m_outputHeight), m_bgLayer.mapSize.x, m_bgLayer.mapSize.y,
							SimpleMath::Vector3(m_mainCharacter.origin.x, m_mainCharacter.origin.y, 1.f),
							SimpleMath::Vector3(m_mainCharacter.origin.x, m_mainCharacter.origin.y, 0.f), 
							m_mainCharacter.speed);

	AUDIO_ENGINE_FLAGS eflags = AudioEngine_Default;
#ifdef _DEBUG
	eflags = eflags | AudioEngine_Debug;
#endif
	m_audEngine = std::make_unique<AudioEngine>(eflags);
	m_retryAudio = false;

	m_soundsWb = std::make_unique<WaveBank>(m_audEngine.get(), L"sounds.xwb");
	m_currentMusic = std::make_unique<SoundEffect>(m_audEngine.get(), L"tristramAD.wav");
	
	m_musicLoop = m_currentMusic->CreateInstance();
	if (m_musicLoop)
	{
		m_musicLoop->SetVolume(.3f);
		m_musicLoop->Play(true);
	}
	std::random_device rd;
	m_random = std::make_unique<std::mt19937>(rd());
    // TODO: Change the timer settings if you want something other than the default variable timestep mode.
    // e.g. for 60 FPS fixed timestep update logic, call:
    
    //m_timer.SetFixedTimeStep(true);
    //m_timer.SetTargetElapsedSeconds(1.0 / 143);   
	
}

// Properties
void Game::GetDefaultSize(int& width, int& height) const
{
	// TODO: Change to desired default window size (note minimum size is 320x200).
	width = 1600;
	height = 900;
}

// Helper method to clear the back buffers.
void Game::Clear()
{
    // Clear the views.
    m_d3dContext->ClearRenderTargetView(m_renderTargetView.Get(), Colors::CornflowerBlue);
    m_d3dContext->ClearDepthStencilView(m_depthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

    m_d3dContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), m_depthStencilView.Get());

    // Set the viewport.
	CD3D11_VIEWPORT viewport(0.f, 0.f, static_cast<float>(m_outputWidth), static_cast<float>(m_outputHeight));
    m_d3dContext->RSSetViewports(1, &viewport);
}

// Presents the back buffer contents to the screen.
void Game::Present()
{
    // The first argument instructs DXGI to block until VSync, putting the application
    // to sleep until the next VSync. This ensures we don't waste any cycles rendering
    // ssFrames that will never be displayed to the screen.
    HRESULT hr = m_swapChain->Present(1, 0);

    // If the device was reset we must completely reinitialize the renderer.
    if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
    {
        OnDeviceLost();
    }
    else
    {
        DX::ThrowIfFailed(hr);
    }
}

// Message handlers
void Game::OnActivated()
{
    // TODO: Game is becoming active window.
}

void Game::OnDeactivated()
{
    // TODO: Game is becoming background window.
}

void Game::OnSuspending()
{
    // TODO: Game is being power-suspended (or minimized).
	m_audEngine->Suspend();
}

void Game::OnResuming()
{
    // TODO: Game is being power-resumed (or returning from minimize).
	m_timer.ResetElapsedTime();
	m_audEngine->Resume();
}

void Game::OnWindowSizeChanged(int width, int height)
{
    m_outputWidth = std::max(width, 1);
    m_outputHeight = std::max(height, 1);
	/*
	DXGI_MODE_DESC myDesc = {};
	myDesc.Width = 800;
	myDesc.Height = 600;
	myDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	HRESULT hr = m_swapChain->ResizeTarget(&myDesc);
	if (hr != S_OK)
		bool test = true;
	*/
    CreateResources();
	if (m_gameState == BATTLE)
	{	
		float offset = (m_battleBg.size.x - static_cast<float>(m_outputWidth)) / 2.f;
		m_camera.OnWindowSizeChange(static_cast<float>(m_outputWidth), static_cast<float>(m_outputHeight), offset);
	}
	else
		m_camera.OnWindowSizeChange(static_cast<float>(m_outputWidth), static_cast<float>(m_outputHeight), m_bgLayer.mapSize.x, m_bgLayer.mapSize.y);
}

void Game::OnDeviceLost()
{
	// TODO: Add Direct3D resource cleanup here.

	m_depthStencilView.Reset();
	m_renderTargetView.Reset();
	m_swapChain.Reset();
	m_d3dContext.Reset();
	m_d3dDevice.Reset();

	m_spriteBatch.reset();
	m_primitiveBatch.reset();
	m_bgLayer.ss.mTexture.Reset();

	for (unsigned int i = 0; i < m_NPCSS.spriteSheets.size(); i++)
	{
		m_NPCSS.spriteSheets[i].mTexture.Reset();
	}

	m_screenTransition.m_states.reset();
	m_screenTransition.m_effect.reset();
	m_screenTransition.m_inputLayout.Reset();
	m_keyboard.reset();
	m_mouse.reset();
	m_font.reset();

	CreateDevice();

	CreateResources();

}

// These are the resources that depend on the device.
void Game::CreateDevice()
{
    UINT creationFlags = 0;

#ifdef _DEBUG
    creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    static const D3D_FEATURE_LEVEL featureLevels [] =
    {
        // TODO: Modify for supported Direct3D feature levels
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
        D3D_FEATURE_LEVEL_9_3,
        D3D_FEATURE_LEVEL_9_2,
        D3D_FEATURE_LEVEL_9_1,
    };

    // Create the DX11 API device object, and get a corresponding context.
    ComPtr<ID3D11Device> device;
    ComPtr<ID3D11DeviceContext> context;
    DX::ThrowIfFailed(D3D11CreateDevice(
        nullptr,                            // specify nullptr to use the default adapter
        D3D_DRIVER_TYPE_HARDWARE,
        nullptr,
        creationFlags,
        featureLevels,
        _countof(featureLevels),
        D3D11_SDK_VERSION,
        device.ReleaseAndGetAddressOf(),    // returns the Direct3D device created
        &m_featureLevel,                    // returns feature level of device created
        context.ReleaseAndGetAddressOf()    // returns the device immediate context
        ));

#ifndef NDEBUG
    ComPtr<ID3D11Debug> d3dDebug;
    if (SUCCEEDED(device.As(&d3dDebug)))
    {
        ComPtr<ID3D11InfoQueue> d3dInfoQueue;
        if (SUCCEEDED(d3dDebug.As(&d3dInfoQueue)))
        {
#ifdef _DEBUG
            d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
            d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);
#endif
            D3D11_MESSAGE_ID hide [] =
            {
                D3D11_MESSAGE_ID_SETPRIVATEDATA_CHANGINGPARAMS,
                // TODO: Add more message IDs here as needed.
            };
            D3D11_INFO_QUEUE_FILTER filter = {};
            filter.DenyList.NumIDs = _countof(hide);
            filter.DenyList.pIDList = hide;
            d3dInfoQueue->AddStorageFilterEntries(&filter);
        }
    }
#endif

    DX::ThrowIfFailed(device.As(&m_d3dDevice));
    DX::ThrowIfFailed(context.As(&m_d3dContext));
	
    // TODO: Initialize device dependent objects here (independent of window size).

	//create map
	m_bgLayer.Load(0, m_gameData, m_d3dDevice.Get());
	m_fgLayer.Load(0, m_gameData, m_d3dDevice.Get());
	m_tLayer.Load(0, m_gameData);

	//create sprites
	m_partySS.AddSS(m_mainCharacter.id, m_gameData, m_d3dDevice.Get());
	m_mainCharacter = m_partySS.spriteSheets[m_mainCharacter.id].CreateSprite(DirectX::XMFLOAT2(3200.f, 1700.f));
	m_party.push_back(m_mainCharacter);
	m_party.push_back(m_partySS.spriteSheets[m_mainCharacter.id].CreateSprite(DirectX::XMFLOAT2(3200.f, 2700.f)));
	m_party.push_back(m_partySS.spriteSheets[m_mainCharacter.id].CreateSprite(DirectX::XMFLOAT2(3200.f, 3700.f)));
	m_PartyAI.AddAI(0, m_gameData);

	m_NPCSS.AddSS(2, m_gameData, m_d3dDevice.Get());
	m_NPCs.push_back(m_NPCSS.spriteSheets[0].CreateSprite(DirectX::XMFLOAT2(2000.f, 1300.f), true, "MedPunchL"));
	m_NPCs.push_back(m_NPCSS.spriteSheets[0].CreateSprite(DirectX::XMFLOAT2(2000.f, 2300.f)));
	m_NPCs.push_back(m_NPCSS.spriteSheets[0].CreateSprite(DirectX::XMFLOAT2(2000.f, 3300.f)));
	m_NPCAI.AddAI(2, m_gameData);
	m_NPCs[0].speed = 13.f;
	m_NPCs[1].speed = 13.f;
	m_NPCs[2].speed = 13.f;
	

	//auto samplerState = states->LinearWrap();
	//m_d3dContext->PSSetSamplers(0, 1, &samplerState);
	/*
	D3D11_SAMPLER_DESC desc = CD3D11_SAMPLER_DESC(CD3D11_DEFAULT());
	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	//desc.MaxAnisotropy = 16;
	m_d3dDevice->CreateSamplerState(&desc, &state);
	m_d3dContext->PSSetSamplers(0, 1, &state);
	*/
	m_screenTransition.Init(m_d3dDevice.Get());
	m_spriteBatch = std::make_unique<SpriteBatch>(m_d3dContext.Get());
	m_primitiveBatch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(m_d3dContext.Get());
	m_font = std::make_unique<SpriteFont>(m_d3dDevice.Get(), L"fof.spritefont");
	
	//states = std::make_unique<DirectX::CommonStates>(m_d3dDevice.Get());
}

// Allocate all memory resources that change on a window SizeChanged event.
void Game::CreateResources()
{
    // Clear the previous window size specific context.
    ID3D11RenderTargetView* nullViews [] = { nullptr };
    m_d3dContext->OMSetRenderTargets(_countof(nullViews), nullViews, nullptr);
    m_renderTargetView.Reset();
    m_depthStencilView.Reset();
    m_d3dContext->Flush();
	
    UINT backBufferWidth = static_cast<UINT>(m_outputWidth);
    UINT backBufferHeight = static_cast<UINT>(m_outputHeight);
    DXGI_FORMAT backBufferFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
    DXGI_FORMAT depthBufferFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
    UINT backBufferCount = 2;

    // If the swap chain already exists, resize it, otherwise create one.
    if (m_swapChain)
    {
		HRESULT hr = m_swapChain->ResizeBuffers(backBufferCount, backBufferWidth, backBufferHeight, backBufferFormat, 0);
        
        if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
        {
            // If the device was removed for any reason, a new device and swap chain will need to be created.
            OnDeviceLost();

            // Everything is set up now. Do not continue execution of this method. OnDeviceLost will reenter this method 
            // and correctly set up the new device.
            return;
        }
        else
        {
            DX::ThrowIfFailed(hr);
        }
    }
    else
    {
        // First, retrieve the underlying DXGI Device from the D3D Device.
        ComPtr<IDXGIDevice1> dxgiDevice;
        DX::ThrowIfFailed(m_d3dDevice.As(&dxgiDevice));

        // Identify the physical adapter (GPU or card) this device is running on.
        ComPtr<IDXGIAdapter> dxgiAdapter;
        DX::ThrowIfFailed(dxgiDevice->GetAdapter(dxgiAdapter.GetAddressOf()));

        // And obtain the factory object that created it.
        ComPtr<IDXGIFactory2> dxgiFactory;
        DX::ThrowIfFailed(dxgiAdapter->GetParent(IID_PPV_ARGS(dxgiFactory.GetAddressOf())));

        // Create a descriptor for the swap chain.
        DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
        swapChainDesc.Width = backBufferWidth;
        swapChainDesc.Height = backBufferHeight;
        swapChainDesc.Format = backBufferFormat;
        swapChainDesc.SampleDesc.Count = 1;
        swapChainDesc.SampleDesc.Quality = 0;
        swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        swapChainDesc.BufferCount = backBufferCount;

        DXGI_SWAP_CHAIN_FULLSCREEN_DESC fsSwapChainDesc = {};
        fsSwapChainDesc.Windowed = TRUE;

        // Create a SwapChain from a Win32 window.
        DX::ThrowIfFailed(dxgiFactory->CreateSwapChainForHwnd(
            m_d3dDevice.Get(),
            m_window,
            &swapChainDesc,
            &fsSwapChainDesc,
            nullptr,
            m_swapChain.ReleaseAndGetAddressOf()
            ));

        // This template does not support exclusive fullscreen mode and prevents DXGI from responding to the ALT+ENTER shortcut.
        DX::ThrowIfFailed(dxgiFactory->MakeWindowAssociation(m_window, DXGI_MWA_NO_ALT_ENTER));
    }

    // Obtain the backbuffer for this window which will be the final 3D rendertarget.
    ComPtr<ID3D11Texture2D> backBuffer;
    DX::ThrowIfFailed(m_swapChain->GetBuffer(0, IID_PPV_ARGS(backBuffer.GetAddressOf())));

    // Create a view interface on the rendertarget to use on bind.
    DX::ThrowIfFailed(m_d3dDevice->CreateRenderTargetView(backBuffer.Get(), nullptr, m_renderTargetView.ReleaseAndGetAddressOf()));

    // Allocate a 2-D surface as the depth/stencil buffer and
    // create a DepthStencil view on this surface to use on bind.
    CD3D11_TEXTURE2D_DESC depthStencilDesc(depthBufferFormat, backBufferWidth, backBufferHeight, 1, 1, D3D11_BIND_DEPTH_STENCIL);

    ComPtr<ID3D11Texture2D> depthStencil;
    DX::ThrowIfFailed(m_d3dDevice->CreateTexture2D(&depthStencilDesc, nullptr, depthStencil.GetAddressOf()));

    CD3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc(D3D11_DSV_DIMENSION_TEXTURE2D);
    DX::ThrowIfFailed(m_d3dDevice->CreateDepthStencilView(depthStencil.Get(), &depthStencilViewDesc, m_depthStencilView.ReleaseAndGetAddressOf()));
    
	// TODO: Initialize windows-size dependent objects here.

}

// Executes the basic game loop.

void Game::Tick()
{
	m_timer.Tick([&]()
	{
		Update(m_timer);
	});

	Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& timer)
{
	float dt = float(timer.GetElapsedSeconds());
	if (dt > .1f)
		dt = .1f;

	// TODO: Add your game logic here.
	if (m_retryAudio)
	{
		m_retryAudio = false;
		if (m_audEngine->Reset())
		{
			// TODO: restart any looped sounds here
			// TODO: restart any looped sounds here
			if (m_musicLoop)
				m_musicLoop->Play(true);
		}
	}
	else if (!m_audEngine->Update())
	{
		if (m_audEngine->IsCriticalError())
		{
			m_retryAudio = true;
		}
	}

	auto kb = m_keyboard->GetState();
	auto mouse = m_mouse->GetState();

	if (m_gameState == BATTLE)
	{
		for (unsigned int i = 0; i < m_NPCs.size(); i++)
		{
			if (m_NPCs[i].id == enemyIndex)
			{
				m_NPCs[i].accumTime += dt;
				m_NPCs[i].seqAccumTime += dt;
				m_NPCs[i].soundAccumTime += dt;
			}
		}

		for (unsigned int i = 0; i < m_party.size(); i++)
		{
			m_party[i].accumTime += dt;
			m_party[i].seqAccumTime += dt;
			m_party[i].soundAccumTime += dt;
		}
		//get gui selection strin
		//set animRangeIndex of string from gui string
		//std::vector<std::string> commands(10);
		int currPlayerIndex = 0;
		if (kb.T && !m_NPCs[currPlayerIndex].inBattleAnimation)
		{
			target = 0;
			dis.x = m_party[target].origin.x - m_NPCs[currPlayerIndex].origin.x + ((m_NPCs[currPlayerIndex].size.x * m_battleScale) / 1.5f);
			dis.y = m_party[target].origin.y - m_NPCs[currPlayerIndex].origin.y;
			retDis.x = dis.x * -1.f;
			retDis.y = dis.y * -1.f;

			m_npcBattleActions.clear();
			m_npcBattleActions.emplace_back("RunL", 0, dis);
			m_npcBattleActions.emplace_back("PunchL", 2);
			m_npcBattleActions.emplace_back("MedPunchL", 1);
			m_npcBattleActions.emplace_back("RhL", 1);
			m_npcBattleActions.emplace_back("RunR", 0, retDis);

			m_NPCs[currPlayerIndex].animRangeFrameIndex = 0;
			m_NPCs[currPlayerIndex].inBattleAnimation = true;
		}
		
		else if (kb.Y && !m_NPCs[currPlayerIndex].inBattleAnimation)
		{
			target = 1;
			dis.x = m_party[target].origin.x - m_NPCs[currPlayerIndex].origin.x + ((m_NPCs[currPlayerIndex].size.x * m_battleScale) / 1.5f);
			dis.y = m_party[target].origin.y - m_NPCs[currPlayerIndex].origin.y;
			retDis.x = dis.x * -1.f;
			retDis.y = dis.y * -1.f;

			m_npcBattleActions.clear();
			m_npcBattleActions.emplace_back("RunL", 0, dis);
			m_npcBattleActions.emplace_back("PunchL", 2);
			m_npcBattleActions.emplace_back("MedPunchL", 1);
			m_npcBattleActions.emplace_back("RhL", 1);
			m_npcBattleActions.emplace_back("RunR", 0, retDis);

			m_NPCs[currPlayerIndex].animRangeFrameIndex = 0;
			m_NPCs[currPlayerIndex].inBattleAnimation = true;
		}

		else if (kb.U && !m_NPCs[currPlayerIndex].inBattleAnimation)
		{
			target = 2;
			dis.x = m_party[target].origin.x - m_NPCs[currPlayerIndex].origin.x + ((m_NPCs[currPlayerIndex].size.x * m_battleScale) / 1.5f);
			dis.y = m_party[target].origin.y - m_NPCs[currPlayerIndex].origin.y;
			retDis.x = dis.x * -1.f;
			retDis.y = dis.y * -1.f;

			m_npcBattleActions.clear();
			m_npcBattleActions.emplace_back("RunL", 0, dis);
			m_npcBattleActions.emplace_back("PunchL", 2);
			m_npcBattleActions.emplace_back("MedPunchL", 1);
			m_npcBattleActions.emplace_back("RhL", 1);
			m_npcBattleActions.emplace_back("RunR", 0, retDis);

			m_NPCs[currPlayerIndex].animRangeFrameIndex = 0;
			m_NPCs[currPlayerIndex].inBattleAnimation = true;
		}

		else if (kb.G && !m_party[currPlayerIndex].inBattleAnimation)
		{
			target = 0;
			DirectX::XMFLOAT2 dis, retDis;
			dis.x = m_NPCs[target].origin.x - m_party[currPlayerIndex].origin.x - ((m_party[currPlayerIndex].size.x * m_battleScale) / 1.5f);
			dis.y = m_party[target].origin.y - m_NPCs[currPlayerIndex].origin.y;
			retDis.x = dis.x * -1.f;
			retDis.y = dis.y * -1.f;
			m_playerBattleActions.clear();
			m_playerBattleActions.emplace_back("RunR", dis);
			m_playerBattleActions.emplace_back("PunchR", 2);
			m_playerBattleActions.emplace_back("MedPunchR", 1);
			m_playerBattleActions.emplace_back("KickR", 1);
			m_playerBattleActions.emplace_back("MedKickR", 1);
			m_playerBattleActions.emplace_back("KickR", 1);
			m_playerBattleActions.emplace_back("BackFlipR", 1);
			m_playerBattleActions.emplace_back("RunL", retDis);

			m_party[currPlayerIndex].animRangeFrameIndex = 0;
			m_party[currPlayerIndex].inBattleAnimation = true;
		}

		else if (kb.H && !m_party[currPlayerIndex].inBattleAnimation)
		{
			target = 1;
			DirectX::XMFLOAT2 dis, retDis;
			dis.x = m_NPCs[target].origin.x - m_party[currPlayerIndex].origin.x - ((m_party[currPlayerIndex].size.x * m_battleScale) / 1.5f);
			dis.y = m_party[target].origin.y - m_NPCs[currPlayerIndex].origin.y;
			retDis.x = dis.x * -1.f;
			retDis.y = dis.y * -1.f;
			m_playerBattleActions.clear();
			m_playerBattleActions.emplace_back("RunR", dis);
			m_playerBattleActions.emplace_back("PunchR", 2);
			m_playerBattleActions.emplace_back("MedPunchR", 1);
			m_playerBattleActions.emplace_back("KickR", 1);
			m_playerBattleActions.emplace_back("MedKickR", 1);
			m_playerBattleActions.emplace_back("KickR", 1);
			m_playerBattleActions.emplace_back("BackFlipR", 1);
			m_playerBattleActions.emplace_back("RunL", retDis);

			m_party[currPlayerIndex].animRangeFrameIndex = 0;
			m_party[currPlayerIndex].inBattleAnimation = true;
		}

		else if (kb.J && !m_party[currPlayerIndex].inBattleAnimation)
		{
			target = 2;
			DirectX::XMFLOAT2 dis, retDis;
			dis.x = m_NPCs[target].origin.x - m_party[currPlayerIndex].origin.x - ((m_party[currPlayerIndex].size.x * m_battleScale) / 1.5f);
			dis.y = m_party[target].origin.y - m_NPCs[currPlayerIndex].origin.y;
			retDis.x = dis.x * -1.f;
			retDis.y = dis.y * -1.f;
			m_playerBattleActions.clear();
			m_playerBattleActions.emplace_back("RunR", dis);
			m_playerBattleActions.emplace_back("PunchR", 2);
			m_playerBattleActions.emplace_back("MedPunchR", 1);
			m_playerBattleActions.emplace_back("KickR", 1);
			m_playerBattleActions.emplace_back("MedKickR", 1);
			m_playerBattleActions.emplace_back("KickR", 1);
			m_playerBattleActions.emplace_back("BackFlipR", 1);
			m_playerBattleActions.emplace_back("RunL", retDis);

			m_party[currPlayerIndex].animRangeFrameIndex = 0;
			m_party[currPlayerIndex].inBattleAnimation = true;
		}
		//update npcs animations
		for (int i = 0; i < m_NPCs.size(); i++)
		{
			if (m_NPCs[i].inBattleAnimation)
			{
				m_NPCAI.AIvec[0].PlayBattleSequenceAnimation(m_NPCs[i], m_party[target], m_npcBattleActions, dt, m_soundsWb, m_enemySound);
			}
			else if (m_NPCs[i].hitStatus > 0 && m_NPCs[i].hitStatus < 4)
			{
				m_NPCAI.AIvec[0].PlayHitStatus(m_NPCs[i], dt, m_soundsWb, m_enemySound, m_playerHitSound);
			}
			else if (m_NPCs[i].hitStatus != static_cast<char>(KO))
				m_NPCAI.AIvec[0].LoopConditionAnimation(m_NPCs[i], dt);
		}
	
		for (int i = 0; i < m_party.size(); i++)
		{
			if (m_party[i].inBattleAnimation)
				m_PartyAI.AIvec[0].PlayBattleSequenceAnimation(m_party[i], m_NPCs[target], m_playerBattleActions, dt, m_soundsWb, m_playerSound);
			else if (m_party[i].hitStatus > 0 && m_party[i].hitStatus < 4)
			{
				m_PartyAI.AIvec[0].PlayHitStatus(m_party[i], dt, m_soundsWb, m_playerSound, m_playerHitSound);
			}
			else if (m_party[i].hitStatus != static_cast<char>(KO))
				m_PartyAI.AIvec[0].LoopConditionAnimation(m_party[i], dt);
		}
	}

	if (m_gameState == FREEROAM)
	{
		ProcessFreeMovementInput(kb, dt);
		m_camera.UpdateCamera();
		//update npcs timers
		for (unsigned int i = 0; i < m_NPCs.size(); i++)
		{
			m_NPCs[i].accumTime += dt;
			m_NPCs[i].seqAccumTime += dt;
		}
		//update npcs animations
		int count = 0;
		for (int i = 0; i < m_NPCSS.spriteSheets.size(); i++)
		{
			for (int j = 0; j < m_NPCSS.spriteSheets[i].unitCount; j++)
			{
				m_NPCAI.AIvec[i].UpdateBehaviorAnimation(m_NPCs[count], dt);
				count++;
			}
		}
		tlIndex = CheckTransitions();
		enemyIndex = CheckEnemyCollision();
	}
	if (m_gameState == BATTLE_ENCOUNTERED)
	{
		if (!m_soundsWb->IsInUse())
		{
			m_gameState = BATTLE_TRANSITION;
		}
	}
	//Check for anything that requires fadein/fadeout
	if (m_gameState == MAP_TRANSITION || m_gameState == BATTLE_TRANSITION)
	{
		m_screenTransition.alphaValue += m_screenTransition.alphaDelta;

		if (m_screenTransition.alphaValue > .99f)
		{
			if (tlIndex > -1)
			{
				float spawnY;

				if (m_tLayer.zoneLines[tlIndex].newSpawn.y == -1.f)
					spawnY = m_mainCharacter.origin.y;
				else
					spawnY = m_tLayer.zoneLines[tlIndex].newSpawn.y;

				DirectX::XMFLOAT2 spawn(m_tLayer.zoneLines[tlIndex].newSpawn.x, spawnY);
				//map change
				m_bgLayer.Load(m_tLayer.zoneLines[tlIndex].drIndex, m_gameData, m_d3dDevice.Get());
				m_fgLayer.Load(m_tLayer.zoneLines[tlIndex].drIndex, m_gameData, m_d3dDevice.Get());
				m_tLayer.Load(m_tLayer.zoneLines[tlIndex].drIndex, m_gameData);
				//delete npc, sprites, ai
				m_NPCSS.Clear();
				m_NPCAI.Clear();
				m_NPCs.clear();
				// load new npc sprites ai
				m_NPCSS.AddSS(2, m_gameData, m_d3dDevice.Get());
				m_NPCs.emplace_back(m_NPCSS.spriteSheets[0].CreateSprite(DirectX::XMFLOAT2(3000.f, 1300.f)));
				m_NPCAI.AddAI(2, m_gameData);
				// position party outside map border area, set camera to party
				m_mainCharacter.Place(spawn);
				m_camera.InitializeCamera(static_cast<float>(m_outputWidth), static_cast<float>(m_outputHeight), m_bgLayer.mapSize.x, m_bgLayer.mapSize.y,
					SimpleMath::Vector3(m_mainCharacter.origin.x, m_mainCharacter.origin.y, 1),
					SimpleMath::Vector3(m_mainCharacter.origin.x, m_mainCharacter.origin.y, 0),
					m_mainCharacter.speed);

				m_camera.SetLocks();

				if (m_camera.bottomLocked)
				{
					m_camera.pos.y = m_camera.margins.bottom;
					m_camera.focus.y = m_camera.margins.bottom;
				}
				else if (m_camera.topLocked)
				{
					m_camera.pos.y = m_camera.margins.top;
					m_camera.focus.y = m_camera.margins.top;
				}
				if (m_camera.leftLocked)
				{
					m_camera.pos.x = m_camera.margins.left;
					m_camera.focus.x = m_camera.margins.left;
				}
				else if (m_camera.rightLocked)
				{
					m_camera.pos.x = m_camera.margins.right;
					m_camera.focus.x = m_camera.margins.right;
				}
				m_camera.UpdateCamera();
				m_gameState = FREEROAM_LOCKEDMOVE;
			}
			else if (enemyIndex > -1)
			{
				//float offset = (m_battleBg.size.x - static_cast<float>(m_outputWidth)) / 2.f;
				m_camera.InitializeCamera(static_cast<float>(m_outputWidth), static_cast<float>(m_outputHeight), m_battleBg.size.x, m_battleBg.size.y,
					SimpleMath::Vector3(m_battleBg.origin.x, m_battleBg.origin.y, 1),
					SimpleMath::Vector3(m_battleBg.origin.x, m_battleBg.origin.y, 0),
					m_mainCharacter.speed);
				m_battleBg.Load(0, m_gameData, m_d3dDevice.Get());
				
				m_party[0].Place(m_battleBg.LeftPos1);
				m_party[1].Place(m_battleBg.LeftPos2);
				m_party[2].Place(m_battleBg.LeftPos3);
				for (unsigned int i = 0; i < m_party.size(); i++)
				{
					m_party[i].speed *= 8;
					m_party[i].animName = "IdleR";
					m_party[i].orientation = 'R';
					m_party[i].condition = IDLE;
					m_party[i].inBattleAnimation = false;
					m_party[i].accumOffset.x = 0.f;
					m_party[i].accumOffset.y = 0.f;
					m_party[i].accumTime = 0.f;
					m_party[i].seqAccumTime = 0.f;
					m_party[i].soundAccumTime = 0.f;
				}

				m_NPCs[0].Place(m_battleBg.RightPos1);
				m_NPCs[1].Place(m_battleBg.RightPos2);
				m_NPCs[2].Place(m_battleBg.RightPos3);
				for (unsigned int i = 0; i < m_NPCs.size(); i++)
				{
					if (m_NPCs[i].id == enemyIndex)
					{
						m_NPCs[i].animName = "IdleL";
						m_NPCs[i].orientation = 'L';
						m_NPCs[i].condition = IDLE;
						m_NPCs[i].inBattleAnimation = false;
						m_NPCs[i].accumOffset.x = 0.f;
						m_NPCs[i].accumOffset.y = 0.f;
						m_NPCs[i].accumTime = 0.f;
						m_NPCs[i].seqAccumTime = 0.f;
						m_NPCs[i].soundAccumTime = 0.f;
					}
				}

				m_gameState = BATTLE;
				m_musicLoop.reset();
				m_currentMusic.reset();
				//m_currentMusic = std::make_unique<SoundEffect>(m_audEngine.get(), L"narutoBattleC.wav");
				m_currentMusic = std::make_unique<SoundEffect>(m_audEngine.get(), L"mkBattleC.wav");
				m_musicLoop = m_currentMusic->CreateInstance();
				m_musicLoop->SetVolume(0.3f);
				m_musicLoop->Play(true);
			}
			m_screenTransition.active = true;
		}
	}
	else if (m_screenTransition.active)
	{
		m_screenTransition.alphaValue -= m_screenTransition.alphaDelta;

		if (m_screenTransition.alphaValue < .8f && m_gameState == FREEROAM_LOCKEDMOVE)
			m_gameState = FREEROAM;

		if (m_screenTransition.alphaValue < .02f)
		{
			m_screenTransition.active = false;
			m_screenTransition.fadeIn = false;
			m_screenTransition.alphaValue = 0.f;
			m_screenTransition.frameCountBlack = 0;
		}
	}
}


// Draws the scene.
void Game::Render()
{
	
	/*if (test > 1440)
	{
		bool stop;
		stop = true;
	}*/

	//Don't try to render anything before the first Update.
	if (m_timer.GetFrameCount() == 0)
	{
		return;
	}

	Clear();
	// TODO: Add your rendering code here.
	m_spriteBatch->SetRotation(DXGI_MODE_ROTATION_UNSPECIFIED);
	m_spriteBatch->Begin(DirectX::SpriteSortMode_Deferred, nullptr, nullptr, nullptr, nullptr, nullptr, m_camera.wvp);

	//if free roam, draw world map, party, and npcs
	if (m_gameState != BATTLE)
	{
		m_bgLayer.Render(m_spriteBatch.get());
		m_fgLayer.Render(m_spriteBatch.get());
		//draw party
		m_partySS.spriteSheets[m_mainCharacter.id].Draw
		(
			m_spriteBatch.get(),
			m_partySS.spriteSheets[m_mainCharacter.id].ssFrames[m_mainCharacter.spriteSheetIndex],
			m_mainCharacter.origin,
			DirectX::Colors::White,
			0.f,
			m_mainCharacter.scale
		);
		//draw npcs
		int count = 0;
		for (unsigned int i = 0; i < m_NPCSS.spriteSheets.size(); i++)
		{
			for (unsigned int j = 0; j < m_NPCSS.spriteSheets[i].unitCount; j++)
			{
				m_NPCSS.spriteSheets[i].Draw
				(
					m_spriteBatch.get(),
					m_NPCSS.spriteSheets[i].ssFrames[m_NPCs[count].spriteSheetIndex],
					m_NPCs[count].origin,
					DirectX::Colors::White,
					0.f,
					m_NPCs[count].scale
				);
				count++;
			}
		}
	}
	
	if (m_gameState == BATTLE_ENCOUNTERED)
	{
		const wchar_t* output = L"Test Your Might!!";
		std::uniform_int_distribution<> dis(-1, 1);
		int offset = dis(*m_random);

		DirectX::XMFLOAT2 fontPos(m_camera.pos.x + offset, m_camera.pos.y - 350.f + offset);
		DirectX::SimpleMath::Vector2 origin = m_font->MeasureString(output) / 2.f;
		m_font->DrawString(m_spriteBatch.get(), output, fontPos + DirectX::SimpleMath::Vector2(1.f, 1.f), DirectX::Colors::Black, 0.f, origin, 2.8f);
		m_font->DrawString(m_spriteBatch.get(), output, fontPos + DirectX::SimpleMath::Vector2(-1.f, 1.f), DirectX::Colors::Black, 0.f, origin, 2.8f);
		m_font->DrawString(m_spriteBatch.get(), output, fontPos + DirectX::SimpleMath::Vector2(-1.f, -1.f), DirectX::Colors::Black, 0.f, origin, 2.8f);
		m_font->DrawString(m_spriteBatch.get(), output, fontPos + DirectX::SimpleMath::Vector2(1.f, -1.f), DirectX::Colors::Black, 0.f, origin, 2.8f);
		m_font->DrawString(m_spriteBatch.get(), output, fontPos, DirectX::Colors::MintCream, 0.f, origin, 2.8f);
	}

	//if battle, draw battle map, TODO: SCALE MAP TO WINDOW SIZE
	if (m_gameState == BATTLE)
	{
		m_spriteBatch->Draw(m_battleBg.mTexture.Get(), DirectX::XMFLOAT2(0.f, 0.f));

		short playerIndex = -1, npcIndex = -1;

		for (unsigned int i = 0; i < m_party.size(); i++)
		{
			if (m_party[i].inBattleAnimation)
			{
				playerIndex = i;
				continue;
			}
			m_partySS.spriteSheets[0].Draw
			(
				m_spriteBatch.get(),
				m_partySS.spriteSheets[m_mainCharacter.id].ssFrames[m_party[i].spriteSheetIndex],
				m_party[i].origin,
				DirectX::Colors::White,
				0.f,
				m_party[i].scale * m_battleScale
			);
		}

		for (unsigned int i = 0; i < m_NPCs.size(); i++)
		{
			if (m_NPCs[i].inBattleAnimation)
			{
				npcIndex = i;
				continue;
			}
				
			m_NPCSS.spriteSheets[0].Draw
			(
				m_spriteBatch.get(),
				m_NPCSS.spriteSheets[0].ssFrames[m_NPCs[i].spriteSheetIndex],
				m_NPCs[i].origin,
				DirectX::Colors::White,
				0.f,
				m_NPCs[i].scale * m_battleScale
			);
		}
		if (playerIndex > -1)
		{
			m_partySS.spriteSheets[0].Draw
			(
				m_spriteBatch.get(),
				m_partySS.spriteSheets[m_mainCharacter.id].ssFrames[m_party[playerIndex].spriteSheetIndex],
				m_party[playerIndex].origin,
				DirectX::Colors::White,
				0.f,
				m_party[playerIndex].scale * m_battleScale
			);
		}
		else if (npcIndex > -1)
		{
			m_NPCSS.spriteSheets[0].Draw
			(
				m_spriteBatch.get(),
				m_NPCSS.spriteSheets[0].ssFrames[m_NPCs[npcIndex].spriteSheetIndex],
				m_NPCs[npcIndex].origin,
				DirectX::Colors::White,
				0.f,
				m_NPCs[npcIndex].scale * m_battleScale
			);
		}

	}
	
	m_spriteBatch->End();

	//draw fadein/fadeout
	if (m_gameState == MAP_TRANSITION || m_gameState == BATTLE_TRANSITION || m_screenTransition.active)
	{
		m_screenTransition.Render(m_primitiveBatch.get(), m_d3dContext.Get());
	}
	Present();
	//test++;
}






/*START OF HELPER FUNCTIONS*/	










bool Game::Collided(BB& a, BB& b)
{
	if (((abs(a.origin.x - b.origin.x) * 2) < (a.size.x + b.size.x)) && ((abs(a.origin.y - b.origin.y)) * 2 < (a.size.y + b.size.y)))
		return true;
	else
		return false;
}

bool Game::CheckCollisions()
{
	if (m_mainCharacter.bb.collideable)
	{
		unsigned int size = m_bgLayer.bb.size();
		for (unsigned int i = 0; i < size; i++)
		{
			if (m_bgLayer.bb[i].collideable &&
			   ((abs(m_bgLayer.bb[i].origin.x - m_mainCharacter.bb.origin.x) * 2) < (m_bgLayer.bb[i].size.x + m_mainCharacter.bb.size.x)) &&
			   ((abs(m_bgLayer.bb[i].origin.y - m_mainCharacter.bb.origin.y)) * 2 < (m_bgLayer.bb[i].size.y + m_mainCharacter.bb.size.y)))
				return true;
		}

		size = m_fgLayer.bb.size();
		for (unsigned int i = 0; i < size; i++)
		{
			if (m_fgLayer.bb[i].collideable &&
				((abs(m_fgLayer.bb[i].origin.x - m_mainCharacter.bb.origin.x) * 2) < (m_fgLayer.bb[i].size.x + m_mainCharacter.bb.size.x)) &&
				((abs(m_fgLayer.bb[i].origin.y - m_mainCharacter.bb.origin.y)) * 2 < (m_fgLayer.bb[i].size.y + m_mainCharacter.bb.size.y)))
				return true;
		}
	}
	return false;
}

int Game::CheckTransitions()
{
	if (m_mainCharacter.bb.collideable)
	{
		unsigned int size = m_tLayer.zoneLines.size();
		for (unsigned int i = 0; i < size; i++)
		{
			if (m_tLayer.zoneLines[i].bb.collideable &&
				((abs(m_tLayer.zoneLines[i].bb.origin.x - m_mainCharacter.bb.origin.x) * 2.f) < (m_tLayer.zoneLines[i].bb.size.x + m_mainCharacter.bb.size.x)) &&
				((abs(m_tLayer.zoneLines[i].bb.origin.y - m_mainCharacter.bb.origin.y)) * 2.f < (m_tLayer.zoneLines[i].bb.size.y + m_mainCharacter.bb.size.y)))
			{
				m_gameState = MAP_TRANSITION;
				return i;
			}
				
		}
	}
	return -1;
}

int Game::CheckEnemyCollision()
{
	if (m_mainCharacter.bb.collideable)
	{
		unsigned int size = m_NPCs.size();
		for (unsigned int i = 0; i < size; i++)
		{
			if (
				m_NPCs[i].bb.collideable &&
				((abs(m_NPCs[i].bb.origin.x - m_mainCharacter.bb.origin.x) * 2) < (m_NPCs[i].bb.size.x + m_mainCharacter.bb.size.x)) &&
				((abs(m_NPCs[i].bb.origin.y - m_mainCharacter.bb.origin.y)) * 2 < (m_NPCs[i].bb.size.y + m_mainCharacter.bb.size.y)))
			{
				m_gameState = BATTLE_ENCOUNTERED;
				m_musicLoop->Stop();
				std::uniform_int_distribution<> dis(SOUNDS_PREBATTLE1, SOUNDS_PREBATTLE4);
				m_soundsWb->Play(dis(*m_random), .7f, 0.f, 0.f);
				return m_NPCs[i].id;
			}
		}
	}
	return -1;
}

void Game::ProcessFreeMovementInput(DirectX::Keyboard::State& kb, float dt)
{
	float run = 0.f;
	bool running = false;
	m_mainCharacter.accumTime += dt;
	m_mainCharacter.soundAccumTime += dt;

	//Directional Checks
	if (kb.LeftShift)
	{
		run = 2.f;
		running = true;
	}
	else
	{
		run = 1.f;
		running = false;
	}

	if (kb.Up || kb.W)
	{
		if (!m_mainCharacter.inBattleAnimation)
		{
			m_mainCharacter.animRangeIndex = 2;
			m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCAnimation(m_mainCharacter);
		}
	
		m_mainCharacter.orientation = 'U';
		m_mainCharacter.pos.y -= m_mainCharacter.speed * run;
		m_mainCharacter.origin.y -= m_mainCharacter.speed * run;
		m_mainCharacter.bb.origin.y -= m_mainCharacter.speed * run;

		if (CheckCollisions())
		{
			//move sprite it would have collided
			m_mainCharacter.pos.y += m_mainCharacter.speed * run;
			m_mainCharacter.origin.y += m_mainCharacter.speed * run;
			m_mainCharacter.bb.origin.y += m_mainCharacter.speed * run;
		}
		//set camera to character pos unless past margin
		if (m_mainCharacter.origin.y >= m_camera.margins.top && m_mainCharacter.origin.y <= m_camera.margins.bottom)
		{
			m_camera.pos.y = m_mainCharacter.origin.y;
			m_camera.focus.y = m_mainCharacter.origin.y;
		}
	}

	if (kb.Down || kb.S)
	{
		if (!m_mainCharacter.inBattleAnimation)
		{
			m_mainCharacter.animRangeIndex = 3;
			m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCAnimation(m_mainCharacter);
		}

		m_mainCharacter.orientation = 'D';
		m_mainCharacter.pos.y += m_mainCharacter.speed * run;
		m_mainCharacter.origin.y += m_mainCharacter.speed * run;
		m_mainCharacter.bb.origin.y += m_mainCharacter.speed * run;

		if (CheckCollisions())
		{
			//Move sprite and camera back if it would have collided
			m_mainCharacter.pos.y -= m_mainCharacter.speed * run;
			m_mainCharacter.origin.y -= m_mainCharacter.speed * run;
			m_mainCharacter.bb.origin.y -= m_mainCharacter.speed * run;
		}
		//move camera if not past margin
		if (m_mainCharacter.origin.y >= m_camera.margins.top && m_mainCharacter.origin.y <= m_camera.margins.bottom)
		{
			m_camera.pos.y = m_mainCharacter.origin.y;
			m_camera.focus.y = m_mainCharacter.origin.y;
		}
	}

	if (kb.Left || kb.A)
	{
		if (!m_mainCharacter.inBattleAnimation && !running)
		{
			m_mainCharacter.animRangeIndex = 1;
			m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCAnimation(m_mainCharacter);
		}
		else if (!m_mainCharacter.inBattleAnimation && running)
		{
			m_mainCharacter.animRangeIndex = 5;
			m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCAnimation(m_mainCharacter);
		}
		m_mainCharacter.orientation = 'L';
		m_mainCharacter.pos.x -= m_mainCharacter.speed * run;
		m_mainCharacter.origin.x -= m_mainCharacter.speed * run;
		m_mainCharacter.bb.origin.x -= m_mainCharacter.speed * run;
		if (CheckCollisions())
		{
			//move sprite back if collided
			m_mainCharacter.pos.x += m_mainCharacter.speed * run;
			m_mainCharacter.origin.x += m_mainCharacter.speed * run;
			m_mainCharacter.bb.origin.x += m_mainCharacter.speed * run;
		}
		//move camera if not past margin
		if (m_mainCharacter.origin.x >= m_camera.margins.left && m_mainCharacter.origin.x <= m_camera.margins.right)
		{
			m_camera.pos.x = m_mainCharacter.origin.x;
			m_camera.focus.x = m_mainCharacter.origin.x;
		}
	}

	if (kb.Right || kb.D)
	{
		if (!m_mainCharacter.inBattleAnimation && !running)
		{
			m_mainCharacter.animRangeIndex = 0;
			m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCAnimation(m_mainCharacter);
		}
		else if (!m_mainCharacter.inBattleAnimation && running)
		{
			m_mainCharacter.animRangeIndex = 4;
			m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCAnimation(m_mainCharacter);
		}
		m_mainCharacter.orientation = 'R';
		m_mainCharacter.pos.x += m_mainCharacter.speed * run;
		m_mainCharacter.origin.x += m_mainCharacter.speed * run;
		m_mainCharacter.bb.origin.x += m_mainCharacter.speed * run;

		if (CheckCollisions())
		{
			//move sprite and camera back if collided
			m_mainCharacter.pos.x -= m_mainCharacter.speed * run;
			m_mainCharacter.origin.x -= m_mainCharacter.speed * run;
			m_mainCharacter.bb.origin.x -= m_mainCharacter.speed * run;
		}
		//move camera if not past margin
		if (m_mainCharacter.origin.x >= m_camera.margins.left && m_mainCharacter.origin.x <= m_camera.margins.right)
		{
			m_camera.pos.x = m_mainCharacter.origin.x;
			m_camera.focus.x = m_mainCharacter.origin.x;
		}		
	}

	if (m_mainCharacter.animRangeIndex >= 2 && m_mainCharacter.animRangeIndex <= 5 && running &&
		(kb.W || kb.A || kb.S || kb.D))
	{
		if (m_mainCharacter.soundAccumTime > .355f)
		{
			if (m_mainCharacter.soundBit)
			{
				m_soundsWb->Play(SOUNDS_STEPLEFT);
				m_mainCharacter.soundBit = false;
			}
			else
			{
				m_soundsWb->Play(SOUNDS_STEPRIGHT);
				m_mainCharacter.soundBit = true;
			}
			m_mainCharacter.soundAccumTime = 0.f;
		}
	}

	if (kb.R && !m_mainCharacter.inBattleAnimation)
	{
		if (m_mainCharacter.orientation == 'L')
			m_mainCharacter.animRangeIndex = 7;
		else
			m_mainCharacter.animRangeIndex = 6;
		m_mainCharacter.inBattleAnimation = true;
		m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
		std::uniform_int_distribution<> dis(SOUNDS_RAIDENCAR, SOUNDS_RAIDENUNDERTAKER);
		int raidenSound = dis(*m_random);
		m_soundsWb->Play(raidenSound);
	}
	
	if (kb.Space && !m_mainCharacter.inBattleAnimation)
	{
		if (m_mainCharacter.orientation == 'L')
			m_mainCharacter.animRangeIndex = 9;
		else
			m_mainCharacter.animRangeIndex = 8;
		m_mainCharacter.inBattleAnimation = true;
		m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
		m_soundsWb->Play(SOUNDS_JUMP);
	}

	if (kb.Q && !m_mainCharacter.inBattleAnimation)
	{
		if (m_mainCharacter.orientation == 'L')
			m_mainCharacter.animRangeIndex = 11;
		else
			m_mainCharacter.animRangeIndex = 10;
		m_mainCharacter.inBattleAnimation = true;
		m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
		std::uniform_int_distribution<> dis(SOUNDS_SWISHPUNCH, SOUNDS_SWISHHPUNCH);
		int attack = dis(*m_random);
		m_soundsWb->Play(attack);
	}

	if (kb.E && !m_mainCharacter.inBattleAnimation)
	{
		if (m_mainCharacter.orientation == 'L')
			m_mainCharacter.animRangeIndex = 13;
		else
			m_mainCharacter.animRangeIndex = 12;
		m_mainCharacter.inBattleAnimation = true;
		m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
		std::uniform_int_distribution<> dis(SOUNDS_SWISHPUNCH, SOUNDS_SWISHHPUNCH);
		int attack = dis(*m_random);
		m_soundsWb->Play(attack);
	}
	if (kb.D1 && !m_mainCharacter.inBattleAnimation)
	{
		if (m_mainCharacter.orientation == 'L')
			m_mainCharacter.animRangeIndex = 19;
		else
			m_mainCharacter.animRangeIndex = 18;
		m_mainCharacter.inBattleAnimation = true;
		m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
		std::uniform_int_distribution<> dis(SOUNDS_SWISHPUNCH, SOUNDS_SWISHHPUNCH);
		int attack = dis(*m_random);
		m_soundsWb->Play(attack);
	}
	if (kb.D4 && !m_mainCharacter.inBattleAnimation)
	{
		if (m_mainCharacter.orientation == 'L')
			m_mainCharacter.animRangeIndex = 17;
		else
			m_mainCharacter.animRangeIndex = 16;
		m_mainCharacter.inBattleAnimation = true;
		m_mainCharacter.animationRefresh = m_PartyAI.GetAnimationRefresh(m_mainCharacter.id, m_mainCharacter.animRangeIndex);
		std::uniform_int_distribution<> dis(SOUNDS_SWISHPUNCH, SOUNDS_SWISHHPUNCH);
		int attack = dis(*m_random);
		m_soundsWb->Play(attack);
	}

	if (m_mainCharacter.inBattleAnimation)
	{
		switch (m_mainCharacter.animRangeIndex)
		{
		case 6:
		case 7:
		case 18:
		case 19:
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCBAnimation(m_mainCharacter, m_camera, dt);
			break;
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 16:
		case 17:
			m_PartyAI.AIvec[m_mainCharacter.id].UpdateMCBAnimation(m_mainCharacter, dt);
		}
	}
	if (m_mainCharacter.animRangeIndex == 8 || m_mainCharacter.animRangeIndex == 9)
		m_mainCharacter.bb.collideable = false;
	else
		m_mainCharacter.bb.collideable = true;
}

void Game::BattleAnimUpdate(DirectX::Keyboard::State& kb, float dt)
{

}