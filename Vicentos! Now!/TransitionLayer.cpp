#include "pch.h"
#include "TransitionLayer.h"


TransitionZone::TransitionZone(DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 sz, bool c, int i, DirectX::XMFLOAT2 spawn)
{
	bb.origin.x = org.x;
	bb.origin.y = org.y;
	bb.size.x = sz.x;
	bb.size.y = sz.y;
	bb.collideable = c;
	bb.id = -1;
	bb.parentTileId = -1;
	bb.ssIndex = -1;
	drIndex = i;
	newSpawn.x = spawn.x;
	newSpawn.y = spawn.y;
}

TransitionZone::TransitionZone()
{
	bb.origin.x = 0.f;
	bb.origin.x = 0.f;
	bb.size.x = 0.f;
	bb.size.y = 0.f;
	bb.collideable = false;
	drIndex = 0;
	newSpawn.x = 0.f;
	newSpawn.y = 0.f;
}


TransitionZone::~TransitionZone()
{
}

void TransitionLayer::Load(int id, DataResource & dr)
{
	zoneLines.clear();
	//load tsx file, populate bounding boxes
	pugi::xml_document tmx;
	if (tmx.load_file(dr.mapTMXFiles[id]))
	{
		pugi::xml_node objGrp = tmx.child("map").find_child_by_attribute("name", "ZoneLines");
		for (pugi::xml_node obj : objGrp.children("object"))
		{
			DirectX::XMFLOAT2 bbOrigin(0.f, 0.f), bbSize(0.f, 0.f), newSpawn(0.f, 0.f);
			float xPos = 0.f, yPos = 0.f;
			int drIdx = -1;
			bool collide = false;

			xPos = obj.attribute("x").as_float();
			yPos = obj.attribute("y").as_float();
			bbSize.x = obj.attribute("width").as_float();
			bbSize.y = obj.attribute("height").as_float();
			bbOrigin.x = xPos + (bbSize.x / 2.f);
			bbOrigin.y = yPos + (bbSize.y / 2.f);
			collide = obj.child("properties").find_child_by_attribute("name", "collide").attribute("value").as_bool();
			drIdx = obj.child("properties").find_child_by_attribute("name", "drIndex").attribute("value").as_int();
			newSpawn.x = obj.child("properties").find_child_by_attribute("name", "newSpawnX").attribute("value").as_float();
			newSpawn.y = obj.child("properties").find_child_by_attribute("name", "newSpawnY").attribute("value").as_float();

			zoneLines.emplace_back(bbOrigin, bbSize, collide, drIdx, newSpawn);
		}
	}
}
