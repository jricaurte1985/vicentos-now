#pragma once
class FgObject
{
public:
	DirectX::XMFLOAT2 size, origin, pos;
	bool collideable;
	int id, ssIndex;
	FgObject();
	FgObject(DirectX::XMFLOAT2 sz, DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 p, bool col, int i, int ssIdx);
	~FgObject();
};

