//
// Game.h
//

#pragma once

#include "Camera.h"
#include "BgLayer.h"
#include "FgLayer.h"
#include "Sprite.h"
#include "SSFactory.h"
#include "AIFactory.h"
#include "ScreenTransition.h"
#include "TransitionLayer.h"
#include "BattleBg.h"
#include "BattleCommand.h"

// A basic game implementation that creates a D3D11 device and
// provides a game loop.
class Game
{
public:

    Game() noexcept;
	~Game();
    // Initialization and management
    void Initialize(HWND window, int width, int height);

    // Basic game loop
    void Tick();
	void ProcessFreeMovementInput(DirectX::Keyboard::State& kb, float dt);
	void Game::BattleAnimUpdate(DirectX::Keyboard::State& kb, float dt);
	bool Collided(BB& a, BB& b);
	bool CheckCollisions();
	int CheckTransitions();
	int Game::CheckEnemyCollision();

    // Messages
    void OnActivated();
    void OnDeactivated();
    void OnSuspending();
    void OnResuming();
    void OnWindowSizeChanged(int width, int height);
	void OnNewAudioDevice() { m_retryAudio = true; }

    // Properties
    void GetDefaultSize( int& width, int& height ) const;
	
private:

    void Update(DX::StepTimer const& timer);
    void Render();

    void Clear();
    void Present();

    void CreateDevice();
    void CreateResources();
    void OnDeviceLost();

	BgLayer												m_bgLayer;
	FgLayer												m_fgLayer;
	TransitionLayer										m_tLayer;
	BattleBg											m_battleBg;
	Sprite												m_mainCharacter;
	Camera												m_camera;
	std::vector<Sprite>									m_NPCs;
	SSFactory											m_NPCSS;
	std::vector<Sprite>									m_party;
	SSFactory											m_partySS;

	AIFactory											m_NPCAI;
	AIFactory											m_PartyAI;
	ScreenTransition									m_screenTransition;
	DX::StepTimer										m_timer;
	DataResource										m_gameData;
	std::vector<BattleCommand>							m_playerBattleActions;
	std::vector<BattleCommand>							m_npcBattleActions;
	DirectX::XMFLOAT2									dis, retDis;
	float												m_battleScale;
	int target;
	//double test = 0;

    // Device resources.
    HWND												m_window;
    int													m_outputWidth;
    int													m_outputHeight;
	char												m_gameState;
	int													tlIndex, enemyIndex;
	bool												m_retryAudio;

    D3D_FEATURE_LEVEL									m_featureLevel;
    Microsoft::WRL::ComPtr<ID3D11Device1>				m_d3dDevice;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext1>		m_d3dContext;

    Microsoft::WRL::ComPtr<IDXGISwapChain1>				m_swapChain;
    Microsoft::WRL::ComPtr<ID3D11RenderTargetView>		m_renderTargetView;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilView>		m_depthStencilView;

	//Keyboard and mouse
	std::unique_ptr<DirectX::PrimitiveBatch<DirectX::VertexPositionColor>> m_primitiveBatch;
	std::unique_ptr<DirectX::SpriteBatch> m_spriteBatch;
	std::unique_ptr<DirectX::Keyboard> m_keyboard;
	std::unique_ptr<DirectX::Mouse> m_mouse;
	std::unique_ptr<DirectX::AudioEngine> m_audEngine;
	std::unique_ptr<DirectX::WaveBank> m_soundsWb;
	std::unique_ptr<DirectX::SoundEffect> m_currentMusic;
	std::unique_ptr<DirectX::SoundEffectInstance> m_musicLoop;
	std::unique_ptr<DirectX::SoundEffectInstance> m_playerSound;
	std::unique_ptr<DirectX::SoundEffectInstance> m_enemySound;
	std::unique_ptr<DirectX::SoundEffectInstance> m_playerHitSound;
	std::unique_ptr<DirectX::SoundEffectInstance> m_enemyHitSound;
	std::unique_ptr<std::mt19937> m_random;
	std::unique_ptr<DirectX::SpriteFont> m_font;
	//ID3D11SamplerState * state;
	//std::unique_ptr<DirectX::CommonStates> states;
	//ID3D11SamplerState* s;
};