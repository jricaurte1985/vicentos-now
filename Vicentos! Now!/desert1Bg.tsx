<?xml version="1.0" encoding="UTF-8"?>
<tileset name="desert1" tilewidth="640" tileheight="480" tilecount="48" columns="6">
 <image source="../../../../Documents/desert1.png" width="3852" height="3856"/>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="0" y="184" width="628" height="30"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index">
   <object id="1" x="288" y="0" width="30" height="480"/>
  </objectgroup>
 </tile>
 <tile id="19">
  <objectgroup draworder="index">
   <object id="1" x="0" y="212" width="296" height="24"/>
   <object id="2" x="284" y="212" width="28" height="268"/>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index">
   <object id="1" x="332" y="192" width="304" height="16"/>
   <object id="2" x="332" y="200" width="20" height="268"/>
   <object id="3" x="360" y="212" width="92" height="80"/>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index">
   <object id="1" x="240" y="0" width="32" height="232"/>
   <object id="2" x="0" y="212" width="268" height="24"/>
  </objectgroup>
 </tile>
 <tile id="22">
  <objectgroup draworder="index">
   <object id="1" x="344" y="180" width="296" height="16"/>
   <object id="2" x="328" y="-4" width="16" height="200"/>
  </objectgroup>
 </tile>
 <tile id="23">
  <objectgroup draworder="index">
   <object id="1" x="0" y="176" width="268" height="16"/>
   <object id="2" x="280" y="0" width="16" height="196"/>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index">
   <object id="1" x="0" y="220" width="132" height="16"/>
   <object id="2" x="140" y="216" width="60" height="128"/>
   <object id="3" x="544" y="256" width="40" height="76"/>
   <object id="5" x="546" y="248" width="90" height="12"/>
  </objectgroup>
 </tile>
 <tile id="25">
  <objectgroup draworder="index">
   <object id="1" x="304" y="352" width="12" height="116"/>
   <object id="2" x="188" y="348" width="132" height="8"/>
   <object id="3" x="148" y="76" width="148" height="12"/>
   <object id="4" x="284" y="0" width="16" height="84"/>
  </objectgroup>
 </tile>
 <tile id="26">
  <objectgroup draworder="index">
   <object id="1" x="164" y="108" width="24" height="92"/>
   <object id="2" x="480" y="120" width="20" height="84"/>
   <object id="3" x="0" y="204" width="188" height="20"/>
   <object id="4" x="480" y="200" width="160" height="16"/>
  </objectgroup>
 </tile>
</tileset>
