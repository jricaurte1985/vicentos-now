#include "pch.h"
#include "Behavior.h"

Behavior::Behavior() {};

Behavior::Behavior(std::vector<DirectX::XMFLOAT2> offsets, std::vector<float> dur, std::vector<int> seq, std::string n)
	: posOffsets(offsets), durations(dur), animSequence(seq), animName(n)
{
}

Behavior::Behavior(const Behavior& obj)
{
	animName = obj.animName;
	animSequence = obj.animSequence;
	posOffsets = obj.posOffsets;
	durations = obj.durations;
}