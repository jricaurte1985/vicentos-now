#pragma once
#include "AnimationRange.h"
#include "Behavior.h"
#include "Camera.h"
#include "BattleCommand.h"

class AI
{
public:
	std::vector<AnimationRange> animRanges;
	std::vector<Behavior> behaviors;
	int id;

	AI();
	~AI();
	void Load(int i, DataResource& dr);
	void UpdateBehaviorAnimation(Sprite& s, std::string behavior, float dt);
	void UpdateBehaviorAnimation(Sprite& s, float dt);
	void LoopConditionAnimation(Sprite & s, float dt);
	void PlayHitStatus(Sprite& s, float dt, std::unique_ptr<DirectX::WaveBank>const &wb, std::unique_ptr<DirectX::SoundEffectInstance> &painSound, std::unique_ptr<DirectX::SoundEffectInstance> &powSound);
	void PlayBattleSequenceAnimation(Sprite& s, Sprite& target, std::vector<BattleCommand> commands, float dt, std::unique_ptr<DirectX::WaveBank>const &wb, std::unique_ptr<DirectX::SoundEffectInstance> &sound);
	void UpdateMCAnimation(Sprite& s);
	void UpdateMCBAnimation(Sprite& s, float dt);
	void UpdateMCBAnimation(Sprite& s, Camera& c, float dt);
	int GetBehaviorIndex(std::string b);
};

