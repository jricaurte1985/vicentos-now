#pragma once

enum GAMESTATES
{
	FREEROAM = 0,
	FREEROAM_LOCKEDMOVE = 1,
	BATTLE_ENCOUNTERED = 2,
	BATTLE_TRANSITION = 3,
	BATTLE = 4,
	MAP_TRANSITION = 5,
	INVENTORY = 6,
	MENU = 7,
	TITLE = 8,
	DEATHSCREEN = 9
};

#define GAMESTATES_ENTRY_COUNT 9

enum BATTLECONDITION
{
	IDLE = 0,
	TIRED = 1,
	WOUNDED = 2,
	CRITICAL = 3,
	DEAD = 4
};
#define BATTLECONDITION_ENTRY_COUNT 5

enum HITSTATUS
{
	NONE = 0,
	HIT_HIGH = 1,
	HIT_MID = 2,
	KO = 3,
	DEFEATED = 4
};
#define HITSTATUS_ENTRY_COUNT 5