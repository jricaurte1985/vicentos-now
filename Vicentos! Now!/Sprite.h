#pragma once
#include "BB.h"
#include "AnimationRange.h"

class Sprite
{
public:
	DirectX::XMFLOAT2 origin, pos, size, accumOffset;
	float scale, speed, animationRefresh, accumTime, soundAccumTime, seqAccumTime;
	int spriteSheetIndex, animRangeIndex, id, animRangeFrameIndex, behaviorSeqIndex, commandIndex, cycleCount, hitPoints;
	BB bb;
	std::string behavior, animName;
	bool inBattleAnimation, soundBit;
	unsigned char orientation, condition, hitStatus, soundIndex;
	
	Sprite();	
	Sprite(int i, DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 p, DirectX::XMFLOAT2 s, BB b, float sp, float animR, std::string beh, std::string aName, float sc, unsigned char o);
	Sprite(const Sprite& obj);
	void Place(DirectX::XMFLOAT2 p);
	void SetBehavior(std::string b);
};

