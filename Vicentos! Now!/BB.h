#pragma once

struct BB
{
	DirectX::XMFLOAT2 origin, size;
	int id;
	int parentTileId;
	int ssIndex;
	
	bool collideable;

	BB();
	BB(DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 sz, int i, int tId, int sIdx, bool c);
};



