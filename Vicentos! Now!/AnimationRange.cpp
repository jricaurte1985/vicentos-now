#include "pch.h"
#include "AnimationRange.h"

AnimationRange::AnimationRange() {};


AnimationRange::AnimationRange(std::vector<DirectX::XMFLOAT2> offsets, std::vector<char> hFrames, std::vector<char> sounds, DirectX::XMFLOAT2 cycOff, std::string s, int mi, int ma, float ref)
	: posOffsets(offsets), hitFrames(hFrames), soundIndices(sounds), cycleOffset(cycOff), name(s), min(mi), max(ma), animRefresh(ref)
{
};

