<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Level0" tilewidth="640" tileheight="480" tilecount="30" columns="6">
 <image source="../../../../Documents/Level0.png" width="3868" height="2424"/>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="640" height="480"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="24" x="0" y="0" width="640" height="413.333"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" x="245" y="0" width="395" height="68"/>
   <object id="3" x="494.667" y="62" width="145.333" height="358"/>
   <object id="7" x="245" y="418" width="425" height="62"/>
  </objectgroup>
 </tile>
</tileset>
