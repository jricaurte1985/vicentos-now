#include "pch.h"
#include "FgLayer.h"


FgLayer::FgLayer()
{
}


FgLayer::~FgLayer()
{
}

void FgLayer::Render(DirectX::SpriteBatch* batch)
{
	unsigned int size = fgObjects.size();
	for (unsigned int i = 0; i < size; i++)
	{
		ss.Draw(batch, ss.ssFrames[drawOrder[i]], fgObjects[i].origin, DirectX::Colors::White, 0.f, 1.f);
	}
}
void FgLayer::Load(int idd, DataResource& dr, ID3D11Device1* dev)
{
	fgObjects.clear();
	bb.clear();
	drawOrder.clear();
	ss.ssType = 3;
	id = idd;
	ss.Load(idd, dev, dr);

	pugi::xml_document tmx;

	//load Tiled map file, populate fgObjects vector
	if (tmx.load_file(dr.mapTMXFiles[id]))
	{
		int firstGID = tmx.child("map").child("tileset").next_sibling().attribute("firstgid").as_int();
		pugi::xml_node objGrp = tmx.child("map").find_child_by_attribute("name", "Foreground");

		int ssIdx = -1, fgId = 0;
		DirectX::XMFLOAT2 fgSize(0.f, 0.f), fgOrigin(0.f, 0.f), fgPos(0.f, 0.f);
		for (pugi::xml_node obj : objGrp.children("object"))
		{
			ssIdx = (obj.attribute("gid").as_int() - firstGID);
			drawOrder.push_back(ssIdx);

			fgSize.x = obj.attribute("width").as_float();
			fgSize.y = obj.attribute("height").as_float();
			fgPos.x = obj.attribute("x").as_float();
			fgPos.y = obj.attribute("y").as_float() - fgSize.y;
			fgOrigin.x = fgPos.x + (fgSize.x / 2.f);
			fgOrigin.y = fgPos.y + (fgSize.y / 2.f);
			fgObjects.emplace_back(fgSize, fgOrigin, fgPos, true, fgId, ssIdx);
			++fgId;
		}
	}

	//load tsx file, populate bounding boxes
	pugi::xml_document tsx;
	if (tsx.load_file(dr.mapFgTSXFiles[id]))
	{
		pugi::xml_node bbTiles = tsx.child("tileset");
		int bbID = 0;
		int numObjs = fgObjects.size();

		DirectX::XMFLOAT2 bbSize(0.f, 0.f), bbOrigin(0.f, 0.f);
		for (pugi::xml_node tile : bbTiles.children("tile"))
		{
			int ssIdx = tile.attribute("id").as_int();
			pugi::xml_node objects = tile.child("objectgroup");
			for (pugi::xml_node obj : objects.children("object"))
			{
				bbSize.x = obj.attribute("width").as_float();
				bbSize.y = obj.attribute("height").as_float();
				float xPos = obj.attribute("x").as_float();
				float yPos = obj.attribute("y").as_float();
				float halfWidth = bbSize.x / 2.f;
				float halfHeight = bbSize.y / 2.f;
				for (int i = 0; i < numObjs; i++)
				{
					if (fgObjects[i].ssIndex == ssIdx)
					{
						int objId = fgObjects[i].id;
						bbOrigin.x = fgObjects[i].pos.x + xPos + halfWidth;
						bbOrigin.y = fgObjects[i].pos.y + yPos + halfHeight;
						bb.emplace_back(bbOrigin, bbSize, bbID, objId, ssIdx, true);
						++bbID;
					}
				}

			}
		}
	}
}
