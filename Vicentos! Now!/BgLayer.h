#pragma once
#include "Tile.h"
#include "BB.h"
#include "pugixml.hpp"

class BgLayer
{
public:
	SpriteSheet ss;
	//background layer, foreground layer, object layer
	std::vector<Tile> tiles;
	std::vector<int> drawOrder;
	//Bounding Boxes
	std::vector<BB> bb;
	
	DirectX::XMFLOAT2 mapSize, tileSize;
	int id;
	
	void Render(DirectX::SpriteBatch* batch);
	void Load(int idd, DataResource& dr, ID3D11Device1* dev);
};
