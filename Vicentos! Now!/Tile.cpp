#include "pch.h"
#include "Tile.h"

Tile::Tile()
{
	origin.x = 0.f;
	origin.y = 0.f;
	pos.x = 0.f;
	pos.y = 0.f;
	id = -1;
	ssIndex = -1;
}

Tile::Tile(DirectX::XMFLOAT2 org, DirectX::XMFLOAT2 p, std::vector<int> bbs, int i, int ssI)
	: origin(org), pos(p), bbIDs(bbs), id(i), ssIndex(ssI)
{
}
