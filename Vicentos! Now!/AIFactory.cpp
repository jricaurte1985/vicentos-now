#include "pch.h"
#include "AIFactory.h"


AIFactory::AIFactory()
{
}


AIFactory::~AIFactory()
{
}

bool AIFactory::Contains(int ssID)
{
	int s = AIvec.size();
	for (int i = 0; i < s; i++)
	{
		if (AIvec[i].id == ssID)
			return true;
	}
	return false;
}

void AIFactory::AddAI(int aiID, DataResource& dr)
{
	if (!Contains(aiID))
	{
		AI ai;
		ai.id = aiID;
		ai.Load(aiID, dr);
		AIvec.emplace_back(ai);
	}
}

void AIFactory::Clear()
{
	AIvec.clear();
}

float AIFactory::GetAnimationRefresh(int spriteId, int animRangeIndex)
{
	int s = AIvec.size();
	for (int i = 0; i < s; i++)
	{
		if (AIvec[i].id == spriteId)
			return AIvec[i].animRanges[animRangeIndex].animRefresh;
	}
}