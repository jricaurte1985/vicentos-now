//--------------------------------------------------------------------------------------
// File: SpriteSheet.h
//
// C++ sprite sheet renderer
//
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//--------------------------------------------------------------------------------------

#pragma once
#include "Sprite.h"

class SpriteSheet
{
public:
	std::vector<SpriteFrame> ssFrames;
	DirectX::XMFLOAT2 unitSize;
	float speed, animRefresh, scale;
	int id, unitCount;
	unsigned int ssType; // 1 is sprite, 2 is bgTiles, 3 is fgObjs, ...
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>    mTexture = nullptr;

	SpriteSheet();
	~SpriteSheet();
	void Load(int i, ID3D11Device1* dev, DataResource& dr);
	Sprite CreateSprite(DirectX::XMFLOAT2 org, bool col = true, std::string beh = "IdleL", unsigned char o = 'L', float sc = 1.f);

    // Draw overloads specifying position and scale as XMFLOAT2.
	void XM_CALLCONV Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::XMFLOAT2 const& position,
		DirectX::FXMVECTOR color = DirectX::Colors::White, float rotation = 0, float scale = 1,
		DirectX::SpriteEffects effects = DirectX::SpriteEffects_None, float layerDepth = 0) const;
   

	void XM_CALLCONV Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::XMFLOAT2 const& position,
		DirectX::FXMVECTOR color, float rotation, DirectX::XMFLOAT2 const& scale,
		DirectX::SpriteEffects effects = DirectX::SpriteEffects_None, float layerDepth = 0) const;
                    
    // Draw overloads specifying position and scale via the first two components of an XMVECTOR.
	void XM_CALLCONV Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::FXMVECTOR position,
		DirectX::FXMVECTOR color = DirectX::Colors::White, float rotation = 0, float scale = 1,
		DirectX::SpriteEffects effects = DirectX::SpriteEffects_None, float layerDepth = 0) const;
   
	void XM_CALLCONV Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, DirectX::FXMVECTOR position,
		DirectX::FXMVECTOR color, float rotation, DirectX::GXMVECTOR scale,
		DirectX::SpriteEffects effects = DirectX::SpriteEffects_None, float layerDepth = 0) const;
   
    // Draw overloads specifying position as a RECT.
	void XM_CALLCONV Draw(DirectX::SpriteBatch* batch, const SpriteFrame& frame, RECT const& destinationRectangle,
		DirectX::FXMVECTOR color = DirectX::Colors::White, float rotation = 0,
		DirectX::SpriteEffects effects = DirectX::SpriteEffects_None, float layerDepth = 0) const;
};